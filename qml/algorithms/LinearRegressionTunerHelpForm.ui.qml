import QtQuick 2.4
import QtQuick.Controls 2.0
import "../imports"

FlickableStackPage {
    id: main
    title: titleText.text

    Column {
        id: column
        width: parent.width
        spacing: 10
        H1Text {
            id: titleText
            width: parent.width
            text: "Dynamic Tuner"
        }

        Text {
            id: text1
            text: qsTr("This tuner uses a sophisticated algorithm to automatically tune the played keys to pure intervals as close as possible. Thus it is possible to dynamically change the key and still play in Just Intonation.")
            wrapMode: Text.WordWrap
            width: parent.width
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
