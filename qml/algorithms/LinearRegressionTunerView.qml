import QtQuick 2.0
import QtQuick.Controls 2.2
import MonteverdiApp 1.0

Page {
    property var menuItem: mainMenuModel.elementByGroupIdx("Temperament", 2)
    anchors.fill: parent
    property int fullHeight: 10

    LinearRegressionTunerAlgorithm {
        id: tuner
    }

    Text {
        id: label
        text: qsTr("An algorithm will automatically adjust the temperament so that it is as close to a pure intonation as possible.")
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }
}
