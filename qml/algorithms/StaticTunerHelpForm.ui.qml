import QtQuick 2.4
import QtQuick.Controls 2.0
import "../imports"

FlickableStackPage {
    id: main
    title: titleText.text

    Column {
        id: column
        width: parent.width
        spacing: 10
        H1Text {
            id: titleText
            width: parent.width
            text: "Static Tuner"
        }

        Text {
            id: text1
            text: qsTr("This tuing scheme will set fixed intervall ratios based on one key. Thus, you can play pure, but only in one key.")
            wrapMode: Text.WordWrap
            width: parent.width
        }

        Text {
            id: text2
            width: parent.width
            text: qsTr("Besides Just Intonation many different temperaments are predefined.")
            wrapMode: Text.WordWrap
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
