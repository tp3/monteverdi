import QtQuick 2.4
import QtQuick.Controls 2.3

Page {
    id: item1

    property alias fullHeight: label.height
    anchors.fill: parent

    Text {
        id: label
        text: qsTr("The differences in frequency between any semitone is fixed. This is the usual tuning of modern western music.")
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }
}
