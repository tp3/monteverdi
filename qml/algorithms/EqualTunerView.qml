import QtQuick 2.4
import MonteverdiApp 1.0

EqualTunerViewForm {
    property var menuItem: mainMenuModel.elementByGroupIdx("Temperament", 0)

    EqualTunerAlgorithm {
        id: tuner
    }
}
