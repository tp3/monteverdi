import QtQuick 2.3
import QtQuick.Controls 2.2
import "qrc:/imports"

FlickableStackPage {
    property var algorithm: null
    property alias currentTemperament: temperamentSelection.currentIndex;
    title: qsTr("Interval Cents")

    function updateIndexCents(index, cent) {
        if (pitchEntries.itemAt(index)) {
            pitchEntries.itemAt(index).value = cent * 100
        }
    }

    Component.onCompleted: {
        algorithm.pitchChanged.connect(updateIndexCents);
    }

    Component.onDestruction: {
        algorithm.pitchChanged.disconnect(updateIndexCents);
    }

    readonly property var intervalNames: [qsTr("Semitone"), qsTr(
            "Major Second"), qsTr("Minor Third"), qsTr("Major Third"), qsTr(
            "Perfect Forth"), qsTr("Tritone"), qsTr("Perfect Fifth"), qsTr(
            "Minor Sixth"), qsTr("Major Sixth"), qsTr("Minor Seventh"), qsTr(
            "Major Seventh"), qsTr("Octave")]

    Column {
        id: layout
        //anchors.fill: parent
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: 300
        height: 40 * 13 + 12 * spacing
        spacing: 2
        Item {
            id: temperamentSelectionGroup
            width: parent.width
            height: 40
            Row {
                anchors.fill: parent
                spacing: 10
                Label {
                    width: 100
                    height: parent.height
                    verticalAlignment: Qt.AlignVCenter
                    text: qsTr("Temperament")
                }

                ComboBox {
                    width: 190
                    height: parent.height
                    id: temperamentSelection
                    model: [qsTr("Equal"), qsTr("Just Intonation"), qsTr("Pythagorean"), qsTr("1/4 Meantone"), qsTr("Werckmeister III"), qsTr("Silbermann 1/6"), qsTr("Zarline -2/7"), qsTr("User defined")];
                    currentIndex: 1
                    onCurrentIndexChanged: {
                        algorithm.updateTemperaments(currentIndex);
                    }
                }
            }
        }

        Repeater {
            id: pitchEntries
            width: parent.width
            model: 12
            Item {
                width: parent.width
                height: 40
                property alias value: cents.value
                Row {
                    spacing: 10
                    anchors.fill: parent
                    Label {
                        height: parent.height
                        width: 100
                        text: intervalNames[index]
                        verticalAlignment: Qt.AlignVCenter
                    }
                    DoubleSpinBox {
                        id: cents
                        height: parent.height
                        width: 190
                        from: -10000
                        to: 10000
                        stepSize: 1
                        editable: true
                        value: 0
                        onRealValueChanged: {
                            algorithm.setPitch(index, realValue);
                            if (temperamentSelection.currentIndex < temperamentSelection.count - 1) {
                                var autoRealValue =  algorithm.temperaments[temperamentSelection.currentIndex]["temperament"][index];
                                if (Math.abs(autoRealValue - realValue) > 0.005) {
                                    temperamentSelection.currentIndex = temperamentSelection.count - 1;
                                }
                            }
                        }
                        Component.onCompleted: {
                            value = algorithm.getPitch(index) * 100;
                            // value = algorithm.temperaments[temperamentSelection.currentIndex]["temperament"][index] * 100;
                        }
                    }
                }
            }
        }
    }
}
