import QtQuick 2.4
import QtQuick.Controls 2.0
import "../imports"

FlickableStackPage {
    id: main
    title: titleText.text

    Column {
        id: column
        width: parent.width
        spacing: 10
        H1Text {
            id: titleText
            text: qsTr("Equal Tuner")
            width: parent.width
        }

        Text {
            id: text1
            text: qsTr("This tuning is the common tuning in todays western music: Any semitone is equidistant. In this tuning scheme you can play any key alike on the drawback that there are no pure intervalls.")
            wrapMode: Text.WordWrap
            width: parent.width
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
