import QtQuick 2.4
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import MonteverdiApp 1.0
import "qrc:/imports"
// import "../../.."

Item {
    property var menuItem: mainMenuModel.elementByGroupIdx("Temperament", 1)
    id: main
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    height: layout.height
    property alias fullHeight: main.height

    StaticTunerAlgorithm {
        id: algorithm

        readonly property var temperaments: [
            // EQUAL TEMPERAMENT
            {"temperament": [0.0,   0.0,     0.0,     0.0,   0.0,    0.0,   0.0,     0.0,    0.0,    0.0,    0.0,   0.0], "wolfsShift": 0},
            // JUST INTONATION
            {"temperament": [11.73, 3.91,   15.64,  -13.69, -1.96,  -9.77,  1.96,   13.69,  -15.64,  -31.17, -11.73,  0.0], "wolfsShift": 0},
            // PYTHAGOREAN TUNING
            {"temperament": [-9.76,  3.91,   -5.87,  7.82,   -1.96,  11.73,  1.96,   -7.82,  5.87,   -3.91,  9.78,  0.0], "wolfsShift": 10},
            // ONE QUARTER MEANTONE
            {"temperament": [17.11, -6.83,  10.27,  -13.69, 3.42,   -20.53, -3.42,  13.69,  -10.26, 6.84,   -17.11, 0.0], "wolfsShift": 10},
            // WERCKMEISTER III
            {"temperament": [-3.91,	3.91,	0.00,	-3.91,	3.91,	0.00,	1.96,	-7.82,	0.00,	1.96,	-1.96,  0.0], "wolfsShift": 0},
            // Silbermann 1/6
            {"temperament": [-13.69,	-3.91,	5.87,	-7.82,	1.96,	-11.73,	-1.96,	-15.64,	-5.87,	3.91,	-9.78, 0.0], "wolfsShift": 0},
            // Zarlino -2/7
            {"temperament": [-29.33,	-8.38,	12.57,	-16.76,	4.19,	-25.14,	-4.19,	-33.52,	-12.57,	8.38,	-20.95, 0.0], "wolfsShift": 0},
        ]

        function updateTemperaments(to) {
            if (to >=0 && to < temperaments.length) {
                for (var i = 0; i < temperaments[to]["temperament"].length; i++) {
                    algorithm.setPitch(i, temperaments[to]["temperament"][i]);
                }
                algorithm.setWolfsIntervalShift(temperaments[to]["wolfsShift"]);
            } else {
                algorithm.setWolfsIntervalShift(0)
            }
        }
    }

    Settings {
        property alias referenceKey: referenceKeySelection.currentIndex
    }

    Column {
        id: layout
        anchors.left: parent.left
        anchors.right: parent.right
        height: 40 + 2 * (40 + spacing) + shortInfo.height + spacing;
        spacing: 2

        Text {
            id: shortInfo
            width: parent.width
            text: qsTr("Specify a reference key for a static tuning. Several different tuning schemes (e.g. just intonation) are available.")
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        Item {
            width: parent.width
            height: 40
            Row {
                anchors.fill: parent
                Label {
                    width: 100
                    height: parent.height
                    id: referenceKeySelectionLabel
                    verticalAlignment: Qt.AlignVCenter
                    text: qsTr("Reference Key")
                }

                ComboBox {
                    width: 160
                    height: parent.height
                    id: referenceKeySelection
                    currentIndex: algorithm.referenceKey;
                    model: [qsTr("C"), qsTr("C#"), qsTr("D"), qsTr("D#"), qsTr(
                            "E"), qsTr("F"), qsTr("F#"), qsTr("G"), qsTr("G#"), qsTr(
                            "A"), qsTr("A#"), qsTr("B")];
                    onCurrentIndexChanged: {
                        algorithm.referenceKey = referenceKeySelection.currentIndex;
                    }
                }
            }
        }

        Item {
            width: parent.width
            height: 40
            Row {
                anchors.fill: parent
                Label {
                    width: 100
                    height: parent.height
                    verticalAlignment: Qt.AlignVCenter
                    text: qsTr("Temperament")
                }

                ComboBox {
                    width: 160
                    height: parent.height
                    id: temperamentSelection
                    model: [qsTr("Equal"), qsTr("Just Intonation"), qsTr("Pythagorean"), qsTr("1/4 Meantone"), qsTr("Werckmeister III"), qsTr("Silbermann 1/6"), qsTr("Zarline -2/7"), qsTr("User defined")];
                    currentIndex: 1
                    onCurrentIndexChanged: {
                        algorithm.updateTemperaments(currentIndex);
                    }
                    Component.onCompleted: {
                        algorithm.updateTemperaments(currentIndex);
                    }
                }
                Settings {
                    property alias temperamentSelection: temperamentSelection.currentIndex
                }
            }
        }

        Item {
            width: parent.width
            height: 40
            Row {
                anchors.fill: parent
                Item {
                    height: 1
                    width: 100
                }

                Button {
                    height: parent.height
                    width: 160

                    text: qsTr("Edit intervalls")
                    onClicked: {
                        var out = stackView.push("qrc:/algorithms/StaticTunerCentsView.qml", {"algorithm": algorithm, "currentTemperament": temperamentSelection.currentIndex});
                        out.Component.destruction.connect(function() {
                            temperamentSelection.currentIndex = out.currentTemperament;
                        });
                    }
                }
            }
        }
    }
}
