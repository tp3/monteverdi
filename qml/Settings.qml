import QtQuick 2.4
import "imports"

FlickableStackPage {
    title: qsTr("Settings")

    SettingsForm {
        anchors.fill: parent
    }
}
