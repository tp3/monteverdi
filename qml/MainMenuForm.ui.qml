import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

Item {
    id: mainMenu
    property int stateDevices: -1
    property int stateTemperament: -1
    property int stateSettings: -1

    ListView {
        id: listView
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.rightMargin: 10
        anchors.bottomMargin: 10
        spacing: 10
        model: mainMenuModel
        section {
            property: "group"
            criteria: ViewSection.FullString
            delegate: Rectangle {
                // color: "#FFFFFF"
                width: parent.width
                height: sectionTitleLabel.height + 30
                Column {
                    anchors.fill: parent
                    anchors.topMargin: 10
                    spacing: 10
                    Text {
                        id: sectionTitleLabel
                        // anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: 16
                        font.bold: true
                        font.capitalization: Font.AllUppercase
                        text: section
                    }

                    Rectangle {
                        height: 1
                        width: parent.width
                        color: '#555555'
                    }
                }
            }
        }
        delegate: Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right

            height: 60
            Rectangle {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                width: 5
                visible: mainMenu["state" + group] === idx
                color: "lightblue"
            }

            Row {
                id: row1
                spacing: 10
                height: 60
                width: parent.width - 5
                Item {
                    width: parent.height
                    height: parent.height

                    Image {
                        anchors.centerIn: parent
                        width: parent.height - 10
                        height: parent.height - 10
                        source: "qrc:/icons/" + iconFilename
                        smooth: true
                        mipmap: true
                    }
                }

                Column {
                    height: parent.height
                    width: parent.width - parent.height - row1.spacing - 5
                    /*Text {
                        id: nameText
                        text: name
                        font.pixelSize: 14
                    }*/
                    MainMenuHeadline {
                        id: nameText
                        fontPixelSize: 14
                        leftArrowVisible: leftArrow
                        midArrowVisible: midArrow
                        rightArrowVisible: rightArrow
                        leftText: inText
                        rightText: outText
                        item: model.get(index)
                    }

                    Text {
                        height: parent.height - nameText.height
                        width: parent.width
                        text: description
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        color: '#555555'
                        font.pixelSize: 12
                    }
                }
            }
            MouseArea {
                id: mouse_area1
                z: 1
                hoverEnabled: false
                anchors.fill: parent
                onClicked: mainMenu["state" + group] = idx
            }
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:3;anchors_height:160;anchors_width:110;anchors_x:0;anchors_y:0}
}
 ##^##*/
