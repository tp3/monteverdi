import QtQuick 2.4
import MonteverdiApp 1.0

VirtualMidiKeyboardInputSynthesizerOutputViewForm {
    id: view
    property var menuItem: mainMenuModel.elementByGroupIdx("Devices", 2)
    property var virtualMidiKeyboard: null
    MidiInputSynthesizerOutputMode {
        id: mode
    }

    /*virtualMidiKeyboard.onSendMidiMessage: {
        mode.onHandleMidiMessage(message);
    }*/
    openVirtualKeyboardButton.onClicked: {
        stackView.push(view.virtualMidiKeyboard);
    }

    Component.onCompleted: {
        outputDevice.enabled = false;
        outputDevice.model = mode.synthesizer.availableDeviceNames();
        outputDevice.currentIndex = outputDevice.find(mode.synthesizer.deviceName);
        outputDevice.enabled = true;
        var component = Qt.createComponent("VirtualMidiKeyboardView.qml");
        if (component.status === Component.Ready) {
            view.virtualMidiKeyboard = component.createObject();
            view.virtualMidiKeyboard.sendMidiMessage.connect(mode.onHandleMidiMessage);
        }
    }

    Component.onDestruction: {
        if (virtualMidiKeyboard) {
            virtualMidiKeyboard.destroy();
        }
    }

    outputDevice.onCurrentTextChanged: {
        if (outputDevice.enabled) {
            mode.synthesizer.setDeviceName(outputDevice.currentText);
        }
    }

    outputDeviceVolume: mode.synthesizer.currentVolume;
}
