import QtQuick 2.4
import MonteverdiApp 1.0

MidiInputSynthesizerOutputViewForm {
    id: mainForm;
    property var menuItem: mainMenuModel.elementByGroupIdx("Devices", 1)

    MidiInputSynthesizerOutputMode {
        id: mode
    }

    midiInput.onMidiMessageReceived: {
        if (message.isNoteOn()) {
            midiInputPowerBar.value = message.volume();
        }

        mode.onHandleMidiMessage(message);
    }

    Component.onCompleted: {
        outputDevice.enabled = false;
        outputDevice.model = mode.synthesizer.availableDeviceNames();
        outputDevice.currentIndex = outputDevice.find(mode.synthesizer.deviceName);
        outputDevice.enabled = true;
    }

    outputDevice.onCurrentTextChanged: {
        if (outputDevice.enabled) {
            mode.synthesizer.setDeviceName(outputDevice.currentText);
        }
    }

    outputDeviceVolume: mode.synthesizer.currentVolume;
}
