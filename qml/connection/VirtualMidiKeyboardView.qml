import QtQuick 2.3
import QtQuick.Controls 2.2

Page {
    id: page;
    title: qsTr("Virtual Keyboard")
    VirtualMidiKeyboard {
        id: virtualMidiKeyboard
        anchors.fill: parent
        onSendMidiMessage: {
            page.sendMidiMessage(message);
        }
    }
    signal sendMidiMessage(var message)
}
