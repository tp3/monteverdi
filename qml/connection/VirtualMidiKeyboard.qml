import QtQuick 2.3
import QtQuick.Controls 2.2
import QtMidi 5.0

// A keyboard for touchscreens, particularly suitable for tablets

Item {
    id: keyboardform

    readonly property int numberOfKeys: 61
    readonly property int numberOfWhiteKeys: 36
    readonly property int numberOfBlackKeys: numberOfKeys - numberOfWhiteKeys
    readonly property int midiOffset: 36

    readonly property double whiteKeyLength: height * 1
    readonly property double blackKeyLength: whiteKeyLength * 0.6
    readonly property double whiteKeyWidth: whiteKeyLength * 0.18
    readonly property double blackKeyFraction: 0.6
    readonly property double blackKeyWidth: whiteKeyWidth * blackKeyFraction
    readonly property variant blackKeyPositions: [0.6,1.8,3.55,4.7,5.85]
    readonly property variant diatonicIndices: [0,2,4,5,7,9,11]
    readonly property variant chromaticIndices: [1,3,6,8,10]

    // List holding the indices of pressed key
    property bool triad: false
    property bool touchscreen: false

    // signal to send a midi message
    signal sendMidiMessage(variant message)

    // function to handle a pressed/released key, will send a midi message
    function onKeyPressedChanged(key, pressed) {
        if (pressed) {
            sendMidiMessage(MidiMessageCreator.noteOn(0, key, 60))
        } else {
            sendMidiMessage(MidiMessageCreator.noteOff(0, key, 0))
        }
    }

    Rectangle {
        id: canvas
        color: "black"
        anchors.fill: parent

        Flickable {
            anchors.fill: parent
            anchors.leftMargin: contentWidth < parent.width ? (parent.width-contentWidth)/2 : 0
            anchors.bottomMargin: parent.height*0.8
            contentHeight: canvas.height
            contentWidth: numberOfWhiteKeys*whiteKeyWidth
            flickableDirection: Flickable.HorizontalFlick

            // Position in the center
            contentX: contentWidth / 2 - width / 2


            // Touch area processing the taps

            /*MultiPointTouchArea {
                anchors.fill: parent
                anchors.topMargin: parent.height*0.2
                maximumTouchPoints: 6
                mouseEnabled: true
                property var actualindices: []
                onTouchUpdated: {
                    actualindices = []
                    for (var i in touchPoints) {
                        var point = touchPoints[i];
                        var relativeX = point.x/whiteKeyWidth
                        var whiteKeyIndex = Math.floor(relativeX)
                        var octaveIndex = Math.floor(whiteKeyIndex / 7)
                        var index = 12*octaveIndex + diatonicIndices[whiteKeyIndex % 7]
                        if (point.y < blackKeyLength-parent.height*0.2) {
                            var blackKeyIndex = Math.floor(relativeX/1.4) % 5
                            var dist = (relativeX - 7*octaveIndex) - blackKeyPositions[blackKeyIndex]
                            if (dist > 0 && dist < blackKeyFraction) index = 12*octaveIndex + chromaticIndices[blackKeyIndex]
                        }
                        if (triad) {
                            actualindices.push(index + midiOffset + 4)
                            actualindices.push(index + midiOffset + 7)
                            actualindices.push(index + midiOffset + 12)
                        }
                        actualindices.push(index + midiOffset)
                    }

                    // Send signal
                    if (touchscreen || indices.length !== actualindices.length)
                    {
                        if ( indices != actualindices)
                        {
                            indices = actualindices;
                            sendTouchscreenKeyboardEvent(indices)
                        }
                    }
                }
            }*/

            // White piano keys

            Row {
                anchors.fill: parent
                Repeater {
                    id: whiteKeys
                    model: numberOfWhiteKeys
                    Item {
                        id: whiteKey
                        property bool pressed: false
                        readonly property int key: midiOffset + diatonicIndices[index % diatonicIndices.length] + Math.floor(index / diatonicIndices.length) * 12
                        height: canvas.height
                        width: whiteKeyWidth

                        Rectangle {
                            gradient: Gradient {
                                GradientStop { position: 0.0; color: "white" }
                                GradientStop { position: 1.0; color: pressed ? "#ddd" : "white" }
                            }
                            border.color: "black"
                            border.width: (pressed ? 0.02 : 0.01) * width
                            anchors.fill: parent
                            anchors.bottomMargin: pressed ? 0 : whiteKeyLength*0.01
                            radius: whiteKeyWidth*0.1
                        }
                        Text {
                            y: parent.height - font.pixelSize - whiteKeyLength*0.04
                            width: parent.width
                            height: font.pixelSize
                            visible: index%7==0
                            font.family: window.font
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            color: "gray"
                            text: "C" + (index/7 + 2)
                        }
                        MultiPointTouchArea {
                            anchors.fill: parent
                            anchors.topMargin: parent.height*0.2
                            maximumTouchPoints: 1
                            mouseEnabled: true
                            onPressed: {
                                if (touchscreen) {
                                    whiteKey.pressed = true;
                                    onKeyPressedChanged(key, whiteKey.pressed)
                                } else {
                                    whiteKey.pressed = !whiteKey.pressed;
                                    onKeyPressedChanged(key, whiteKey.pressed)
                                }

                            }
                            onReleased: {
                                if (touchscreen) {
                                    whiteKey.pressed = false;
                                    onKeyPressedChanged(key, whiteKey.pressed)
                                }
                            }
                        }
                    }
                }
            }

            // Black piano keys

            Repeater {
                id: blackKeys
                model: numberOfBlackKeys
                Item {
                    id: blackKey
                    height: blackKeyLength
                    width: blackKeyWidth
                    property bool pressed: false
                    readonly property int key: midiOffset + chromaticIndices[index % chromaticIndices.length] + Math.floor(index / chromaticIndices.length) * 12
                    x: (blackKeyPositions[index%5]+7*Math.floor(index/5))*whiteKeyWidth
                    Rectangle {
                        anchors.fill: parent
                        color: "darkgray"
                        border.color:  "black"
                        border.width: whiteKeyWidth*(pressed ? 0.04:0.02)
                        radius: whiteKeyWidth*0.08
                    }

                    Rectangle {
                        anchors.fill: parent
                        anchors.bottomMargin: parent.height*(pressed ? 0.01 :0.035)
                        anchors.leftMargin: parent.width*0.04
                        anchors.rightMargin: parent.width*0.04
                        gradient: Gradient {
                            GradientStop { position: 0.0; color: "#222" }
                            GradientStop { position: 1.0; color: pressed ? "black" : "#222" }
                        }
                        border.color:  pressed ? "gray" : "lightgray"
                        border.width: whiteKeyWidth*0.04
                        radius: whiteKeyWidth*0.2
                    }
                    MultiPointTouchArea {
                        anchors.fill: parent
                        anchors.topMargin: parent.height*0.2
                        maximumTouchPoints: 1
                        mouseEnabled: true
                        onPressed: {
                            if (touchscreen) {
                                blackKey.pressed = true;
                                onKeyPressedChanged(key, blackKey.pressed)
                            } else {
                                blackKey.pressed = !blackKey.pressed;
                                onKeyPressedChanged(key, blackKey.pressed)
                            }

                        }
                        onReleased: {
                            if (touchscreen) {
                                blackKey.pressed = false;
                                onKeyPressedChanged(key, blackKey.pressed)
                            }
                        }
                    }
                }
            }
        }

        // Black bar seperating claviature from upper part

        Rectangle {
            id: blackbar
            color: "black"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: Math.max(40, parent.height * 0.02)  // determines the width of
        }

        Text {
            anchors.fill: blackbar
            color: "lightgray"
            text: "↔"
            font.family: window.font
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: blackbar.height
        }
    }
}
