import QtQuick 2.4
import QtMidi 5.0
import MonteverdiApp 1.0

MidiInputMidiOutputViewForm {
    id: mainForm
    property var menuItem: mainMenuModel.elementByGroupIdx("Devices", 0)

    MidiInputMidiOutputMode {
        id: mode
        onOutputMidiMessage: {
            if (midiConnectedSwitch.checked) {
                if (message.isNoteOn()) {
                    midiOutputPowerBar.value = message.volume();
                }

                midiOutput.sendMessage(message);
            }
        }
    }


    Component.onCompleted: {
    }

    Component.onDestruction: {
        unmuteOutputDevice();
    }

    midiInput.onMidiMessageReceived: {
        if (message.isNoteOn()) {
            midiInputPowerBar.value = message.volume();
        }

        if (midiConnectedSwitch.checked) {
            mode.onHandleMidiMessage(message);
        }
    }

    midiConnectedSwitch.onCheckedChanged: {
        if (midiConnectedSwitch.checked) {
            muteOutputDevice();
        } else {
            unmuteOutputDevice();
        }
    }

    midiOutput.onCurrentDeviceChanged: {
        if (midiConnectedSwitch.checked) {
            muteOutputDevice();
        }
    }

    midiOutput.onCurrentDeviceToBeDeleted: {
        unmuteOutputDevice();
    }

    midiOutput.onCurrentDeviceDetached: {
        if (midiConnectedSwitch.checked) {
            var messageBoxCombonent = Qt.createComponent("MessageBox.qml");
            var mb = messageBoxCombonent.createObject(mainForm);
            mb.title = qsTr("Warning")
            mb.message = qsTr("MIDI device was disconnected before it was reset to the original state. As a result your device might be muted. You should reconnect the device and disconnect the switch first.")
            mb.open();
        }
    }

    function muteOutputDevice() {
        midiOutput.sendMessage(MidiMessageCreator.disableLocalControl(0))
    }

    function unmuteOutputDevice() {
        midiOutput.sendMessage(MidiMessageCreator.enableLocalControl(0))
    }
}
