import QtQuick 2.4
import QtMidi 5.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: page
    anchors.fill: parent
    readonly property int fullHeight: inputGroup.height + inputPowerBar.height
                                      + connectedSwitch.height
                                      + outputGroup.height + outputPowerBar.height

    property alias midiInput: midiInput
    property alias midiInputPowerBar: inputPowerBar
    property alias midiOutput: midiOutput
    property alias midiOutputPowerBar: outputPowerBar
    property alias midiConnectedSwitch: connectedSwitch

    Column {
        id: column
        anchors.fill: parent

        Item {
            id: inputGroup
            height: midiInput.height
            anchors.right: parent.right
            anchors.left: parent.left

            Image {
                id: inputImage
                width: 20
                height: 20
                anchors.verticalCenter: midiInput.verticalCenter
                source: "qrc:/icons/arrow_tr.svg"
                mipmap: true
            }

            MidiInputComboBox {
                id: midiInput
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.left: inputImage.right
                anchors.right: parent.right
            }
        }

        MidiPowerBar {
            id: inputPowerBar
            anchors.leftMargin: 20
            anchors.right: inputGroup.right
            anchors.left: inputGroup.left
        }

        Switch {
            id: connectedSwitch
            text: checked ? qsTr("Connected") : qsTr("Disconnected")
            checked: true
            anchors.left: parent.left
            anchors.right: parent.right
        }

        Item {
            id: outputGroup
            height: midiOutput.height
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: 0

            Image {
                id: outputImage
                width: 20
                height: 20
                source: "qrc:/icons/arrow_lb.svg"
                mipmap: true
                anchors.verticalCenter: midiOutput.verticalCenter
            }

            MidiOutputComboBox {
                id: midiOutput
                x: 0
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.left: outputImage.right
                Layout.fillWidth: true
            }
        }
        MidiPowerBar {
            id: outputPowerBar
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: 20
            value: 0.5
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
