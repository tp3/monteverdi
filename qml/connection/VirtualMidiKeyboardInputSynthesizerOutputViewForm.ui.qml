import QtQuick 2.4
import QtQuick.Controls 2.2

Page {
    id: main

    anchors.fill: parent

    readonly property int fullHeight: openVirtualKeyboardButton.height
                                      + outputGroup.height + outputVolumeBar.height
    property alias outputDevice: outputDevice
    property alias outputDeviceVolume: outputVolumeBar.value
    property alias openVirtualKeyboardButton: openVirtualKeyboardButton

    Button {
        id: openVirtualKeyboardButton
        text: qsTr("Open Virutal Keyboard")
        anchors.right: parent.right
        anchors.left: parent.left
    }

    Column {
        id: column
        anchors.top: openVirtualKeyboardButton.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left

        Item {
            id: outputGroup
            height: outputDevice.height
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: 0

            Image {
                id: outputImage
                width: 20
                height: 20
                anchors.verticalCenter: outputDevice.verticalCenter
                source: "qrc:/icons/arrow_lb.svg"
                mipmap: true
            }

            ComboBox {
                id: outputDevice
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.left: outputImage.right
            }
        }
        ProgressBar {
            id: outputVolumeBar
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: 20
            value: 0
        }
    }

    // property alias virtualMidiKeyboard: virtualMidiKeyboard

    // VirtualMidiKeyboard {
    //     id: virtualMidiKeyboard
    //     anchors.fill: parent
    // }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
