import QtQuick 2.4
import QtQuick.Controls 2.2

Dialog {
    title: "Info"
    property alias message: message.text
    standardButtons: Dialog.Ok

    width: Math.min(parent.width - 50, 600)
    height: 250
    x: (parent.width - width) / 2
    y: Math.max((parent.height + 50) / 2 - height / 2 - 50, 0)
    dim: true
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent


    Text {
        id: message
        text: qsTr("Information message.")
        anchors.fill: parent
        wrapMode: Text.Wrap
    }
}
