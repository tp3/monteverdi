import QtQuick 2.9
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import MonteverdiApp 1.0

ApplicationWindow {
    id: window
    visible: true
    minimumWidth: 300
    width: 350
    height: 480
    title: qsTr("Monteverdi - The - App")

    property alias stackView: stackView

    Settings {
        property alias currentDevice: mainMenu.stateDevices
        property alias currentAlgorithm: mainMenu.stateTemperament
    }

    Translator {
        id: translator
    }

    property alias mainMenuModel: mainMenuModel
    MainMenuListModel {
        id: mainMenuModel
    }

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        Rectangle {
            anchors.fill: parent
            color: "#164450"
        }


        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            contentItem: Text {
                text: toolButton.text
                font: toolButton.font
                color: "#d7eef4"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            color: "#d7eef4"
            anchors.centerIn: parent
        }

    }

    Drawer {
        id: drawer
        width: Math.max(Math.min(400, window.width), window.width * 0.66)
        height: window.height
        MainMenu {
            id: mainMenu
            anchors.fill: parent
            onActionFinished: drawer.close()
            onStateDevicesChanged: {
                switch (stateDevices) {
                case 0:
                    stackView.get(0).setDevice("qrc:/connection/MidiInputMidiOutputView.qml")
                    break;
                case 1:
                    stackView.get(0).setDevice("qrc:/connection/MidiInputSynthesizerOutputView.qml")
                    break;
                case 2:
                    stackView.get(0).setDevice("qrc:/connection/VirtualMidiKeyboardInputSynthesizerOutputView.qml")
                    break;
                }

            }
            onStateTemperamentChanged: {
                switch (stateTemperament) {
                case 0:
                    stackView.get(0).setTemperament("qrc:/algorithms/EqualTunerView.qml");
                    break;
                case 1:
                    stackView.get(0).setTemperament("qrc:/algorithms/StaticTunerView.qml");
                    break;
                case 2:
                    stackView.get(0).setTemperament("qrc:/algorithms/LinearRegressionTunerView.qml");
                    break;
                }

            }
            onStateSettingsChanged: {
                switch (stateSettings) {
                case 0:
                    stackView.push("Settings.qml");
                    break;
                case 1:
                    stackView.push("help/Help.qml");
                    break;
                case 2:
                    stackView.push("About.qml");
                    break;
                }

                stateSettings = -1;
            }
        }
    }

    Component.onCompleted: {
        // Default view
        // stackView.push("About.qml")

        if (mainMenu.stateTemperament < 0 ) mainMenu.stateTemperament = 0;
        if (mainMenu.stateDevices < 0) mainMenu.stateDevices = 0;
    }

    StackView {
        id: stackView
        initialItem: "Home.qml"
        anchors.fill: parent
    }

    function showHelpPage(filePath) {
        stackView.push(filePath)
    }
}
