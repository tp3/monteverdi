import QtQuick 2.0
import QtQuick.Controls 2.2

Page {
    id: main
    default property alias contents: placeholder.data

    Flickable {
        anchors.fill: parent
        anchors.margins: 10
        contentHeight: placeholder.height
        Rectangle {
            id: placeholder
            width: parent.width
            color: "red"
        }
    }
}
