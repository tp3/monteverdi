import QtQuick 2.0

Text {
    font.pixelSize: Qt.application.font.pixelSize * 1.2
    font.bold: true
}
