import QtQuick 2.0

import QtQuick 2.0
import QtQuick.Controls 2.2

Page {
    id: main
    default property alias contents: placeholder.data

    Item {
        anchors.fill: parent
        anchors.margins: 10
        Item {
            id: placeholder
            anchors.fill: parent
        }
    }
}
