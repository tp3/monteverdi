import QtQuick 2.4
import QtQuick.Controls 2.1

ListModel {
    function elementByGroupIdx(group, index) {
        for (var i = 0; i < count; ++i) {
            var elem = get(i);
            if (elem.group === group && elem.idx === index) {
                return elem;
            }
        }
        console.error("Unknown group and index", group, index)
        return null;
    }

    ListElement {
        leftArrow: false
        midArrow: true
        rightArrow: false
        inText: qsTr("MIDI In")
        outText: qsTr("MIDI Out")
        iconFilename: "midi_in_midi_out.png"
        description: qsTr("Connect the app with your MIDI keyboard.")
        group: "Devices"
        idx: 0
        help: "qrc:/help/MidiInputMidiOutputHelp.qml"
    }

    ListElement {
        leftArrow: false
        midArrow: true
        rightArrow: false
        inText: qsTr("MIDI In")
        outText: qsTr("Synth Out")
        iconFilename: "midi_in_synth_out.png"
        description: qsTr("Play a synthesized sound controlled by a MIDI keyboard")
        group: "Devices"
        idx: 1
        help: "qrc:/help/MidiInputSynthesizerOutputHelp.qml"
    }

    ListElement {
        leftArrow: false
        midArrow: true
        rightArrow: false
        inText: qsTr("Virtual Keyboard")
        outText: qsTr("Synth Out")
        iconFilename: "virtual_keyboard_in_synth_out.png"
        description: qsTr("Play a synthesized sound controlled by a Virtual onscreen keyboard")
        group: "Devices"
        idx: 2
        help: "qrc:/help/VirtualKeyboardInputSynthesizerOutputHelp.qml"
    }

    ListElement {
        leftArrow: false
        midArrow: false
        rightArrow: false
        inText: qsTr("Equal temperament")
        outText: ""
        iconFilename: "equal_temperament.svg"
        description: qsTr("Standard keyboard temperament. Possible to play in all keys but impure")
        group: "Temperament"
        idx: 0
        help: "qrc:/algorithms/EqualTunerHelp.qml"
    }

    ListElement {
        leftArrow: false
        midArrow: false
        rightArrow: false
        inText: qsTr("Static temperament")
        outText: ""
        iconFilename: "static_temperament.svg"
        description: qsTr("Select a temperament relative to one fixed key")
        group: "Temperament"
        idx: 1
        help: "qrc:/algorithms/StaticTunerHelp.qml"
    }

    ListElement {
        leftArrow: false
        midArrow: false
        rightArrow: false
        inText: qsTr("Dynamic temperament")
        outText: ""
        iconFilename: "dynamic_temperament.svg"
        description: qsTr("Dynamically adjust the key depending on the currently played keys.")
        group: "Temperament"
        idx: 2
        help: "qrc:/algorithms/LinearRegressionTunerHelp.qml"
    }

    ListElement {
        leftArrow: false
        midArrow: false
        rightArrow: false
        inText: qsTr("Settings")
        outText: ""
        iconFilename: "settings.svg"
        description: qsTr("Change the default program settings")
        group: "Settings"
        idx: 0
        help: ""
    }

    ListElement {
        leftArrow: false
        midArrow: false
        rightArrow: false
        inText: qsTr("Help")
        outText: ""
        iconFilename: "help.svg"
        description: qsTr("Get help about the app and just intonation.")
        group: "Settings"
        idx: 1
        help: ""
    }

    ListElement {
        leftArrow: false
        midArrow: false
        rightArrow: false
        inText: qsTr("About")
        outText: ""
        iconFilename: "about.svg"
        description: qsTr("Information about the application and disclaimer.")
        group: "Settings"
        idx: 2
        help: ""
    }
}
