import QtQuick 2.4

HomeForm {
    onCurrentDeviceItemChanged: {
        if (currentDeviceItem) {
            deviceHeadline.updateFromDict(currentDeviceItem);
        }
    }

    onCurrentTemperamentItemChanged: {
        if (currentTemperamentItem) {
            temperamentHeadline.updateFromDict(currentTemperamentItem);
        }
    }

    function setDevice(path) {
        if (deviceComponent) {
            deviceComponent.destroy();
        }

        var deviceComponentCreator;
        function createDevice(path) {
            deviceComponentCreator = Qt.createComponent(path)
            if (deviceComponentCreator.status === Component.Error) {
                console.log(deviceComponentCreator.errorString())
            } else if (deviceComponentCreator.status === Component.Ready)
                finishCreation();
            else {
                deviceComponentCreator.statusChanged.connect(finishCreation);
            }
        }

        function finishCreation() {
            var component = deviceComponentCreator;
            if (component.status === Component.Ready) {
                deviceComponent = component.createObject(deviceContent, {"width": "parent.width"});
                deviceContent.height = deviceComponent.fullHeight;
                currentDeviceItem = deviceComponent.menuItem;
            } else if (component.status === Component.Error) {
                console.log("Error loading component:", component.errorString());
            }
        }

        createDevice(path);
    }

    function setTemperament(path) {
        if (temperamentComponent) {
            temperamentComponent.destroy();
        }

        var temperamentCompontentCreator;
        function createDevice(path) {
            temperamentCompontentCreator = Qt.createComponent(path)
            if (temperamentCompontentCreator.status === Component.Error) {
                console.log(temperamentComponentCreator.errorString())
            } else if (temperamentCompontentCreator.status === Component.Ready)
                finishCreation();
            else {
                temperamentCompontentCreator.statusChanged.connect(finishCreation);
            }
        }

        function finishCreation() {
            var component = temperamentCompontentCreator;
            if (component.status === Component.Ready) {
                temperamentComponent = component.createObject(temperamentContent, {"width": "parent.width"});
                temperamentContent.height = temperamentComponent.fullHeight;
                currentTemperamentItem = temperamentComponent.menuItem;
            } else if (component.status === Component.Error) {
                console.log("Error loading component:", component.errorString());
            }
        }

        createDevice(path);
    }
}
