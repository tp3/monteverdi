import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1

Item {
    property alias font: inTextElem.font
    property alias fontPixelSize: inTextElem.font.pixelSize
    property alias fontBold: inTextElem.font.bold
    property alias leftArrowVisible: leftArrow.visible
    property alias midArrowVisible: midArrow.visible
    property alias rightArrowVisible: rightArrow.visible
    property alias leftText: inTextElem.text
    property alias rightText: outTextElem.text
    property alias helpVisible: helpIcon.visible
    property var item: null
    height: inTextElem.height
    width: parent.width

    onItemChanged: {
        if (item) {
            updateFromDict(item);
        }
    }

    function updateFromDict(d) {
        if (d) {
            updateFull(d['leftArrow'], d['inText'], d['midArrow'], d['outText'], d['rightArrow']);
        } else {
            console.warn("Null object for headline!");
        }
    }

    function updateFull(laVisible, i, maVisible, o, raVisible) {
        leftArrowVisible = laVisible;
        midArrowVisible = maVisible;
        rightArrowVisible = raVisible;
        leftText = i;
        rightText = o;
    }

    function setTextOnly(text) {
        updateFull(false, text, false, "", false);
    }

    Row {
        width: parent.width - height
        height: parent.width
        id: row
        spacing: 5
        Image {
            id: leftArrow
            width: height
            height: inTextElem.height
            source: "qrc:/icons/arrow_tr.svg"
            smooth: true
            mipmap: true
        }
        Text {
            id: inTextElem
        }
        Image {
            id: midArrow
            width: height
            height: inTextElem.height
            source: "qrc:/icons/arrow_lr.svg"
            smooth: true
            mipmap: true
        }
        Text {
            id: outTextElem
            font: inTextElem.font
        }
        Image {
            id: rightArrow
            width: height
            height: inTextElem.height
            source: "qrc:/icons/arrow_lb.svg"
            smooth: true
            mipmap: true
        }

    }
    MouseArea {
        id: helpIcon
        visible: false
        anchors.right: parent.right
        width: height
        height: parent.height
        Image {
            anchors.fill: parent
            source: "qrc:/icons/help.svg"
            smooth: true
            mipmap: true
        }

        onClicked: {
            showHelpPage(item.help);
        }
    }
}
