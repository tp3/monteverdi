import QtQuick 2.4

Item {
    id: mainMenu
    property int stateDevices: -1
    property int stateTemperament: -1
    property int stateSettings: -1

    property string displayTextDevices: qsTr("Devices")
    property string displayTextTemperament: qsTr("Temperament")
    property string displayTextSettings: qsTr("Settings and help")

    ListView {
        id: listView
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.rightMargin: 10
        anchors.bottomMargin: 10
        spacing: 10
        model: mainMenuModel
        section {
            property: "group"
            criteria: ViewSection.FullString
            delegate: Rectangle {
                // color: "#FFFFFF"
                width: parent.width
                height: sectionTitleLabel.height + 30
                Column {
                    anchors.fill: parent
                    anchors.topMargin: 10
                    spacing: 10
                    Text {
                        id: sectionTitleLabel
                        // anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: Qt.application.font.pixelSize * 1.2
                        font.bold: true
                        font.capitalization: Font.AllUppercase
                        text: mainMenu["displayText" + section]
                    }

                    Rectangle {
                        height: 1
                        width: parent.width
                        color: '#555555'
                    }
                }
            }
        }
        delegate: Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right

            height: 60
            Rectangle {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                width: 5
                visible: mainMenu["state" + group] === idx
                color: "#afdde9"
            }

            Row {
                id: row1
                spacing: 10
                height: 60
                width: parent.width - 5
                Item {
                    width: parent.height
                    height: parent.height

                    Image {
                        anchors.centerIn: parent
                        width: parent.height - 10
                        height: parent.height - 10
                        source: "qrc:/icons/" + iconFilename
                        smooth: true
                        mipmap: true
                    }
                }

                Column {
                    height: parent.height
                    width: parent.width - parent.height - row1.spacing - 5
                    /*Text {
                        id: nameText
                        text: name
                        font.pixelSize: 14
                    }*/
                    MainMenuHeadline {
                        id: nameText
                        fontPixelSize: Qt.application.font.pixelSize * 1.1
                        item: mainMenuModel.get(index)
                    }

                    Text {
                        height: parent.height - nameText.height
                        width: parent.width
                        text: description
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        color: '#555555'
                        font.pixelSize: Qt.application.font.pixelSize * 0.9
                    }
                }
            }
            MouseArea {
                id: mouse_area1
                z: 1
                hoverEnabled: false
                anchors.fill: parent
                onClicked: mainMenu["state" + group] = idx
            }
        }
    }

    onStateDevicesChanged: {
        actionFinished();
    }
    onStateTemperamentChanged: {
        actionFinished();
    }
    onStateSettingsChanged: {
        actionFinished();
    }

    signal actionFinished()
}
