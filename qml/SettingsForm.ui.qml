import QtQuick 2.4
import QtQuick.Controls 2.3

Page {
    id: container


    Grid {
        id: grid
        width: 300
        layoutDirection: Qt.LeftToRight
        columns: 2
        spacing: 10
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter

        Label {
            id: languageLabel
            text: qsTr("Language")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            height: languageComboBox.height
        }

        ComboBox {
            id: languageComboBox
            model: translator.languages
            currentIndex: translator.index
            onCurrentIndexChanged: if (count > 0) translator.index = currentIndex
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
