import QtQuick 2.4
import "imports"

FlickableStackPage {
    property string versionDate: "2018.09.01"

    title: qsTr("About")
    AboutForm {
        anchors.fill: parent
        title.text: qsTr("Monteverdi The App")
        version.text: qsTr("Build %1").arg(versionDate)
        basedOn.text: "<html>"
                      + qsTr("based on %1, and %2")
                        .arg('<a href="https://qt.io">Qt</a>')
                        .arg('<a href="https://gitlab.com/tp3/qtmidi">QtMidi</a>')
                      + "</html>"
        basedOn.onLinkActivated: Qt.openUrlExternally(link)
    }
}
