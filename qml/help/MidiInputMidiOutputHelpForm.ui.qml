import QtQuick 2.4
import QtQuick.Controls 2.0
import "../imports"

FlickableStackPage {
    id: main
    title: qsTr("MIDI In MIDI Out")

    Column {
        id: column
        width: parent.width
        spacing: 10
        H1Text {
            text: main.title
            width: parent.width
        }

        Text {
            id: text1
            text: qsTr("Select MIDI In MIDI Out if you intend to connect the app to your midi keyboard or to any other MIDI devices that shall be used for input or output. This is the typical use case of this app.")
            wrapMode: Text.WordWrap
            width: parent.width
        }

        Text {
            id: text2
            width: parent.width
            text: qsTr("<ol><li>Connect your MIDI keyboard with a MIDI cable to your device.</li><li>Check if the app automatically selects your device as new input and output MIDI device.</li><li>Use your keyboard to make music</li></ol>")
            wrapMode: Text.WordWrap
        }
    }
}


/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
