import QtQuick 2.3
import "../imports"

StackPage {
    id: helpPage
    title: qsTr("Help - Index")

    ListView {
        anchors.fill: parent

        flickableDirection: Flickable.VerticalFlick

        id: list
        model: helpListModel

        spacing: 1

        delegate: Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right

            height: 60

            Text {
                anchors.fill: parent
                anchors.leftMargin: 10
                verticalAlignment: Text.AlignVCenter
                text: label
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: Qt.application.font.pixelSize * 1.2
            }

            MouseArea {
                id: mouse_area1
                z: 1
                hoverEnabled: false
                anchors.fill: parent
                onClicked: {
                    stackView.push(page)
                }
            }
        }
    }

    ListModel {
        id: helpListModel
        ListElement {
            label: qsTr("Introduction")
            page: "IntroductionHelp.qml"
        }
        ListElement {
            label: qsTr("Midi In Midi Out")
            page: "MidiInputMidiOutputHelp.qml"
        }
        ListElement {
            label: qsTr("MIDI Input Synthesizer Output")
            page: "MidiInputSynthesizerOutputHelp.qml"
        }
        ListElement {
            label: qsTr("Virtual Keyboard Input Synthesizer Output")
            page: "VirtualKeyboardInputSynthesizerOutputHelp.qml"
        }
        ListElement {
            label: qsTr("Equal Tuning")
            page: "../algorithms/EqualTunerHelp.qml"
        }
        ListElement {
            label: qsTr("Static Tuning")
            page: "../algorithms/StaticTunerHelp.qml"
        }
        ListElement {
            label: qsTr("Dynamic Tuning")
            page: "../algorithms/LinearRegressionTunerHelp.qml"
        }
    }
}
