import QtQuick 2.4
import QtQuick.Controls 2.0
import "../imports"

FlickableStackPage {
    id: main
    title: headline.text

    Column {
        id: column
        width: parent.width
        spacing: 10
        H1Text {
            id: headline
            width: parent.width
            text: qsTr("Virtual Keyboard In Synthesizer Out")
        }

        Text {
            id: text1
            text: qsTr("If you have no MIDI keyboard available but still want to try the different  tunings of this application a virtual on-screen keyboard is offered. The sounds will be produced by an inbuild sound generator.")
            wrapMode: Text.WordWrap
            width: parent.width
        }
    }
}


/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
