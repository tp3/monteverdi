import QtQuick 2.0
import MonteverdiApp 1.0

Item {
    property alias filePath: file.filePath
    id: helpPage
    property string title: qsTr("Help")

    FileReader {
        id: file
    }

    Flickable {
        anchors.fill: parent
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        contentWidth: width
        contentHeight: textContent.height

        Text {
            width: parent.width
            id: textContent
            text: file.fileContent
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }
    }
}
