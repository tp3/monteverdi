import QtQuick 2.4
import QtQuick.Controls 2.0
import "../imports"

FlickableStackPage {
    id: main
    title: qsTr("MIDI In Synthesizer Out")

    Column {
        id: column
        width: parent.width
        spacing: 10
        H1Text {
            text: main.title
            width: parent.width
        }

        Text {
            id: text1
            text: qsTr("This connection mode can be used to connect a MIDI keyboard but play a generated sound by the app.")
            wrapMode: Text.WordWrap
            width: parent.width
        }
    }
}


/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
