import QtQuick 2.4
import QtQuick.Controls 2.0
import "../imports"

FlickableStackPage {
    id: main
    title: titleText.text

    Column {
        id: column
        width: parent.width
        spacing: 10
        H1Text {
            id: titleText
            text: qsTr("Introduction")
            width: parent.width
        }

        Text {
            id: text1
            text: qsTr("This app offers a solution to the problem that on a piano it is impossible to play all keys in pure intonation. A dynamic tuning algorithm detects the played keys and slightly chanes its pitches to receive frequency ratios of the intervals that are pure if possible.")
            wrapMode: Text.WordWrap
            width: parent.width
        }

        Text {
            id: text2
            width: parent.width
            text: qsTr("The app is best used with a connected MIDI keyboard: The pressed keys are sent to the app and processed to determine the small pitch variations. This new tuning is sent back to the keyboard to generate the sound in pure intonation.")
            wrapMode: Text.WordWrap
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
