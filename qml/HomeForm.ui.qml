import QtQuick 2.9
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0

Page {
    id: page
    title: qsTr("Home")
    property var deviceComponent: null
    property var temperamentComponent: null
    property alias deviceContent: deviceContent
    property alias temperamentContent: temperamentContent
    property alias currentDeviceItem: deviceHeadline.item
    property alias currentTemperamentItem: temperamentHeadline.item
    property alias deviceHeadline: deviceHeadline
    property alias temperamentHeadline: temperamentHeadline

    Flickable {
        id: flickable
        width: 300
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.bottomMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        flickableDirection: Flickable.VerticalFlick
        contentHeight: columnLayout.height

        Column {
            id: columnLayout
            width: parent.width
            spacing: 10

            MainMenuHeadline {
                id: deviceHeadline
                fontPixelSize: Qt.application.font.pixelSize * 1.2
                fontBold: true
                helpVisible: true
            }

            Rectangle {
                width: parent.width
                height: 1
                color: '#000000'
            }

            Item {
                id: deviceContent
                height: 100
                width: parent.width
            }

            Item {
                id: spacing1
                height: 1
                width: parent.width
            }

            MainMenuHeadline {
                id: temperamentHeadline
                font: deviceHeadline.font
                helpVisible: true
            }

            Rectangle {
                width: parent.width
                height: 1
                color: '#000000'
            }

            Item {
                id: temperamentContent
                height: 100
                width: parent.width
            }
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:3;anchors_height:300;anchors_y:9}D{i:5;anchors_width:200}
}
 ##^##*/
