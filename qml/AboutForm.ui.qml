import QtQuick 2.4

Item {
    property alias title: title
    property alias basedOn: basedOn
    property alias version: version

    Column {
        id: column
        anchors.fill: parent
        spacing: 10

        Item {
            id: item1
            width: parent.width
            height: 80

            Image {
                id: logo
                height: 80
                anchors.left: parent.right
                anchors.leftMargin: -80
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 0
                source: "qrc:/logo/logo.svg"
                mipmap: true
                smooth: true
            }

            Column {
                id: column1
                anchors.right: logo.left
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                spacing: 10

                Text {
                    id: title
                    font.pixelSize: 18
                    text: "Placeholder App Name"
                    wrapMode: Text.WordWrap
                    font.bold: true
                }

                Text {
                    id: version
                    text: "Placeholder App Version"
                    wrapMode: Text.WordWrap
                }

                Text {
                    id: basedOn
                    text: "Placeholder Based On"
                    wrapMode: Text.WordWrap
                }
            }
        }

        Text {
            id: disclaimer
            width: parent.width
            text: qsTr("The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.")
            wrapMode: Text.WordWrap
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:4;anchors_width:100;anchors_x:259;anchors_y:42}
D{i:5;anchors_width:200}D{i:1;anchors_height:400;anchors_width:200;anchors_x:28;anchors_y:23}
}
 ##^##*/
