import QtQuick 2.0
import QtQuick.Controls 2.2

Item {
    id: indicator
    height: 30
    width: 30

    FontLoader { id: musicFont; source: "/Quivira.otf" }

    Item {
        id: notes
        anchors.fill: parent

        Component.onCompleted: {
            for (var i = 0; i < 10; i++) {
                Qt.createQmlObject('import QtQuick.Controls 2.2; Label {id: note' + (i * 2) + '; text: "\uD834\uDD58"; ' +
                                   'font.pointSize: 30; font.family: musicFont.name; anchors.top: parent.top; ' +
                                   'anchors.topMargin: ' + i * -7.5 + "; }",
                                   notes,
                                   "note"
                                   );
                Qt.createQmlObject('import QtQuick.Controls 2.2; Label {id: note' + ((i * 2) + 1) + '; text: "\uD834\uDD58"; ' +
                                   'font.pointSize: 30; font.family: musicFont.name; anchors.top: parent.top; ' +
                                   'anchors.horizontalCenter: parent.horizontalCenter; ' +
                                   'anchors.topMargin: ' + (i * -7.5 - 3.75) + "; }",
                                   notes,
                                   "note"
                                   );
            }
        }
    }

    Label {
        id: upperStaff
        text: "𝄚"
        anchors.fill: parent
        font.pointSize: 30
    }

    function update(noteState) {
        var note = noteState.key - 48;
        if (note >= 0 && note < 10) {
            notes.children[note].visible = noteState.pressed;
        }
    }

}
