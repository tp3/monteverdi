#ifndef WAVEFORMGENERATOR_H
#define WAVEFORMGENERATOR_H

#include <mutex>
#include <QVector>
#include <QTimer>

#include "units/cents.h"
#include "waveform.h"

class WaveformGenerator : public QIODevice
{
    Q_OBJECT
public:
    explicit WaveformGenerator(float baseVolume, int sampleRate, quint8 channelCount,
                               qint64 bufferSize, QObject *parent = nullptr);

    virtual qint64 readData(char *data, qint64 maxlen) override;
    virtual qint64 writeData(const char *, qint64) override
    {
        return 0;
    }                                                                    // dummy

public slots:
    void playNote(int key, Cents cents, double volume);
    void preCalculateWaveform();

signals:
    void currentVolumeChanged(float volume);

private:
    Waveform m_waveforms[128];
    const int m_sampleRate;
    const quint8 m_channelCount;
    const float m_baseVolume;
    const qint64 m_bufferSize;
    float m_currentVolume;
};

#endif // WAVEFORMGENERATOR_H
