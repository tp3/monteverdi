#include "waveform.h"

#include <QDebug>
#include <QtGlobal>
#include <QtMath>

QVector<qint32> Waveform::mSineSuperposition;
bool Waveform::mInitialized = false;

void Waveform::initSineSuperposition()
{
    if (mInitialized) return;
    const int N = 0x10000;
    mSineSuperposition.resize(N);
    // This lambda function defines the spectrum of the partials
    auto f = [](double x) {
                 return std::sin(x) + 0.8 * std::sin(2 * x) + 0.5 * std::sin(3 * x) + 0.2
                        * std::sin(4 * x) + 0.3 * std::sin(5 * x);
             };
    for (int i = 0; i < N;
         ++i) mSineSuperposition[i] = static_cast<qint32>(0x4000 * f(2 * M_PI * i / N));
    mInitialized = true;
}

Waveform::Waveform () :
    mBuffer(1024*2, 0),
    mReadIndex(0),
    mWriteIndex(0),
    mFrequency(0),
    mSampleRate(0),
    mVolume(0),
    mDelay(0),
    mActualVolume(0),
    mVolumeIncrease(0),
    mPhase(0),
    mPhaseDifference(0),
    mPhaseIncrement(0),
    mKeyIsPressed(false)
{
    if (!mInitialized) initSineSuperposition();
}

void Waveform::pressKey(double frequency, int sampleRate, double volume, double stereoDelay)
{
    if (sampleRate == 0) qCritical() << "Sample rate must not be zero";
    mFrequency = frequency;
    mSampleRate = sampleRate;
    mPhaseIncrement = frequency/sampleRate;
    mVolume = static_cast<qint32>(volume*0x1000);
    mPhaseDifference = frequency*stereoDelay;
    mActualVolume = std::max(1, mActualVolume);
    mVolumeIncrease = std::max(1, mVolume/16);
    mKeyIsPressed = true;
    prepareSamples();
}

void Waveform::releaseKey()
{
    mKeyIsPressed = false;
}

bool Waveform::isAudible() const
{
    return mActualVolume > 0;
}

void Waveform::prepareSamples()
{
    while (prepareNextSample()) {}
}

void Waveform::addNextSample(qint32 &left, qint32 &right)
{
    if (mReadIndex == mWriteIndex) prepareNextSample();
    if (mReadIndex % (mSampleRate / 256) == 0) {
        if (mKeyIsPressed) {
            if (mActualVolume < mVolume+mVolumeIncrease) mActualVolume += mVolumeIncrease;
        } else {
            if (mActualVolume > 0) mActualVolume -= std::max(1, mActualVolume/128);
            else mActualVolume = 0;
        }
    }
    left += mBuffer[mReadIndex] * mActualVolume;
    mReadIndex = (mReadIndex+1) % mBuffer.size();
    right += mBuffer[mReadIndex] * mActualVolume;
    mReadIndex = (mReadIndex+1) % mBuffer.size();
}

bool Waveform::prepareNextSample()
{
    QMutexLocker lock(&mBufferMutex);
    const int N = mSineSuperposition.size();
    int successor = (mWriteIndex + 2) % mBuffer.size();
    if (mReadIndex == successor) return false;
    mBuffer[mWriteIndex] = mSineSuperposition[static_cast<qint32>(mPhase * N) % N];
    mBuffer[mWriteIndex
            +1]
        = mSineSuperposition[static_cast<qint32>((mPhase + 1000 + mPhaseDifference) * N) % N];
    mPhase += mPhaseIncrement;
    mPhase -= std::floor(mPhase);
    mWriteIndex = successor;
    return true;
}
