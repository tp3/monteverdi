#ifndef WAVEFORM_H
#define WAVEFORM_H

#include <QVector>
#include <QMutex>

#include <QFile>
#include <QTextStream>

///////////////////////////////////////////////////////////////////////////////
/// \brief Waveform of a single note used by the synthesizer
/// \details This class holds the data of a single periodic wave form
/// and manages a very simple ASR-envelope.
///////////////////////////////////////////////////////////////////////////////

class Waveform
{
public:
    Waveform();
    static void initSineSuperposition();

    void pressKey(double frequency, int sampleRate, double volume, double stereoDelay = 0);
    void releaseKey();
    bool isAudible() const;
    void prepareSamples();

    void addNextSample(qint32 &left, qint32 &right);

private:
    QVector<qint32> mBuffer;
    QMutex mBufferMutex;
    int mReadIndex, mWriteIndex;
    double mFrequency;
    int mSampleRate;
    qint32 mVolume;
    int mDelay;
    qint32 mActualVolume;
    qint32 mVolumeIncrease;
    double mPhase, mPhaseDifference, mPhaseIncrement;
    bool mKeyIsPressed;

private:
    bool prepareNextSample();

    // Static vector holding a sine superposition, available for all instances
    static QVector<qint32> mSineSuperposition;
    static bool mInitialized;
};

#endif // WAVEFORM_H
