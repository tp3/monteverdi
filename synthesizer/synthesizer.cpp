#include "synthesizer.h"

#include <QDebug>
#include <QBuffer>
#include <QTimer>

#include "waveformgenerator.h"
#include <QSettings>

Synthesizer::Synthesizer (QObject *parent) :
    QThread(parent),
    m_availableDeviceInfos(QAudioDeviceInfo::availableDevices(QAudio::AudioOutput)),
    m_audioOutput(nullptr),
    m_waveformGenerator(nullptr),
    m_waveformGeneratorThread(new QThread),
    m_timer(nullptr)
{
    // Start synthesizer (including QAudio) in its own thread with high priority
    QSettings s;
    setDeviceName(s.value("outputDeviceName", QAudioDeviceInfo::defaultInputDevice().deviceName()).toString());
}

Synthesizer::~Synthesizer()
{
    quit();
    wait();
    if (m_waveformGeneratorThread) {
        delete m_waveformGeneratorThread;
        m_waveformGeneratorThread = nullptr;
    }
}

void Synthesizer::playNote(int key, Cents cents, double volume)
{
    m_waveformGenerator->playNote(key, cents, volume);
}

void Synthesizer::setCurrentVolume(double volume)
{
    volume = std::min(1.0, std::max(0.0, volume));
    if (m_currentVolume != volume) {
        m_currentVolume = volume;
        emit currentVolumeChanged();
    }
}

void Synthesizer::setDeviceName(QString name) {
    if (m_deviceInfo.deviceName() == name) {return;}
    quit();
    wait();

    m_deviceInfo = QAudioDeviceInfo();
    for (auto device : m_availableDeviceInfos) {
        if (device.deviceName() == name) {
            m_deviceInfo = device;
            break;
        }
    }

    if (m_deviceInfo.isNull()) {
        m_deviceInfo = QAudioDeviceInfo::defaultOutputDevice();
    }

    QSettings s;
    s.setValue("outputDeviceName", m_deviceInfo.deviceName());
    start(QThread::HighPriority);
}

QStringList Synthesizer::availableDeviceNames() const {
    QStringList l;
    for (auto d : m_availableDeviceInfos) {
        if (d.isFormatSupported(d.preferredFormat())) {
            l << d.deviceName();
        }
    }
    return l;
}

void Synthesizer::run()
{
    QAudioFormat format;
    format.setSampleRate(44100);
    format.setChannelCount(2);
    format.setSampleSize(sizeof(int16_t) * 8);
    format.setCodec("audio/pcm");
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(QAudioFormat::SignedInt);

    QAudioDeviceInfo info(m_deviceInfo);
    if (!info.isFormatSupported(format)) {
        qWarning() << "Raw audio format not supported. Using fallback";
        format = info.nearestFormat(format);
        format.setSampleSize(sizeof(int16_t) * 8);
        qWarning() << "Tying format: " << format;
    }

    if (!info.isFormatSupported(format)) {
        qWarning() << "Raw audio format not supported";
    } else {
        m_deviceInfo = info;
        m_audioFormat = format;
        m_audioOutput = new QAudioOutput(info, format);
        m_audioOutput->start();

        const int buffers_size_ms = 100;
        m_audioOutput->setBufferSize((format.sampleSize() / 8 * format.sampleRate()
                                      * format.channelCount()) * buffers_size_ms / 1000.0);

        m_waveformGenerator = new WaveformGenerator(m_baseVolume,
                                                    format.sampleRate(),
                                                    format.channelCount(),
                                                    m_audioOutput->bufferSize());
        connect(m_waveformGenerator, &WaveformGenerator::currentVolumeChanged, this,
                &Synthesizer::setCurrentVolume);
        m_waveformGenerator->moveToThread(m_waveformGeneratorThread);
        m_waveformGeneratorThread->start();
        m_waveformGenerator->open(QIODevice::ReadOnly);

        m_timer = new QTimer;
        connect(m_timer, &QTimer::timeout, m_waveformGenerator,
                &WaveformGenerator::preCalculateWaveform);
        m_timer->start(10);

        m_audioOutput->start(m_waveformGenerator);
        m_audioOutput->setVolume(1);

        emit deviceNameChanged();

        // Main execution loop for QAudioInput
        exec();

        // Clean up everything
        if (m_timer) {
            m_timer->stop();
            delete m_timer;
        }
        if (m_audioOutput) {
            m_audioOutput->stop();
            delete m_audioOutput;
        }

        if (m_waveformGeneratorThread) {
            m_waveformGeneratorThread->quit();
            m_waveformGeneratorThread->wait();
        }
    }
}
