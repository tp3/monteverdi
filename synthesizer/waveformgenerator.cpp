#include "waveformgenerator.h"

#include <QThread>
#include <QtGlobal>
#include <cmath>
#include <numeric>
#include <limits>
#include <qdebug.h>

#include "units/frequency.h"

WaveformGenerator::WaveformGenerator(float baseVolume, int sampleRate, quint8 channelCount,
                                     qint64 bufferSize, QObject *parent) :
    QIODevice(parent),
    m_sampleRate(sampleRate),
    m_channelCount(channelCount),
    m_baseVolume(baseVolume),
    m_bufferSize(bufferSize),
    m_currentVolume(0)
{
}

qint64 WaveformGenerator::readData(char *data, qint64 maxlen)
{
    const qint64 len = qMin(m_bufferSize, maxlen);
    if (len == 0) return 0;

    QVector<qint32> sum(len/2, 0);
    for (Waveform &w : m_waveforms) if (w.isAudible())
            for (int i = 0; i < len/2; i += 2) w.addNextSample(sum[i], sum[i+1]);
    int16_t *samples = reinterpret_cast<int16_t *>(data);
    const qint32 divisor = static_cast<qint32>(0x4000 / m_baseVolume);
    float volume = 0;
    for (int i = 0; i < len/2; i++) {
        samples[i] = static_cast<int16_t>((sum[i])/divisor);
        const float s = samples[i] * 1.f / std::numeric_limits<int16_t>::max() / m_channelCount
                        / m_baseVolume * 20;
        volume += s * s;
    }
    const float dt = len * 1.f / m_sampleRate / m_channelCount;
    const float deltaV = volume / len / 2 - m_currentVolume;
    m_currentVolume = qMin(1.f, qMax(0.f, m_currentVolume + deltaV * dt * 4));
    emit currentVolumeChanged(m_currentVolume);
    return len;
}

void WaveformGenerator::playNote(int key, Cents cents, double volume)
{
    if (key < 0 || key > 127) {
        qCritical() << "Key index out of range";
        return;
    }
    double f = 440 * pow(2.0, (key-69)/12.0+cents.cents/1200.0);
    if (volume > 0) m_waveforms[key].pressKey(f, 44100, volume, 0.0003*(key-69));
    else m_waveforms[key].releaseKey();
}

void WaveformGenerator::preCalculateWaveform()
{
    Waveform::initSineSuperposition();
    for (Waveform &w: m_waveforms) if (w.isAudible()) w.prepareSamples();
}
