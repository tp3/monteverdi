#ifndef SYNTHESIZER_H
#define SYNTHESIZER_H

#include <QObject>
#include <QAudioOutput>
#include <QThread>
#include <QTimer>

#include "units/cents.h"
#include "waveformgenerator.h"

class Synthesizer : public QThread
{
    Q_OBJECT

    Q_PROPERTY(double currentVolume READ currentVolume WRITE setCurrentVolume NOTIFY currentVolumeChanged)
    Q_PROPERTY(QString deviceName READ deviceName WRITE setDeviceName NOTIFY deviceNameChanged)
public:
    Synthesizer(QObject *parent = 0);

    ~Synthesizer();

    double currentVolume() const {return m_currentVolume;}
    QString deviceName() const {return m_deviceInfo.deviceName();}
    Q_INVOKABLE QStringList availableDeviceNames() const;

signals:
    void requestData(int numberOfSamples);
    void currentVolumeChanged();
    void deviceNameChanged();

public slots:
    void playNote(int key, Cents cents, double volume);
    void setCurrentVolume(double volume);
    void setDeviceName(QString name);

private:
    virtual void run() override;
private:
    QList<QAudioDeviceInfo> m_availableDeviceInfos;
    QAudioDeviceInfo m_deviceInfo;
    QAudioFormat m_audioFormat;
    QAudioOutput *m_audioOutput;
    WaveformGenerator *m_waveformGenerator;
    QThread *m_waveformGeneratorThread;
    QTimer *m_timer;

    double m_currentVolume = 0;
    const double m_baseVolume = 0.5;
};

#endif // SYNTHESIZER_H
