#ifndef QMLINTERFACE_H
#define QMLINTERFACE_H

#include <QObject>
#include <QtMidi/qmidimessage.h>

class QmlInterface : public QObject
{
    Q_OBJECT
public:
    QmlInterface(QObject *parent = 0);

signals:
    void receiveMidiMessage(const QMidiMessage &message);


};

#endif // QMLINTERFACE_H
