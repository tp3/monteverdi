#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>

#include "qmlinterface.h"
#include "config.h"
#include "system/translator.h"
#include "system/filereader.h"
#include "tuner/tuner.h"
#include "tuner/midiinputmidioutputmode.h"
#include "tuner/midiinputsynthesizeroutputmode.h"
#include "synthesizer/synthesizer.h"
#include "tuner/algorithm/tuneralgorithmrunner.h"
#include "tuner/algorithm/tuneralgorithmworker.h"

#include "tuner/algorithm/equaltuner/equaltuneralgorithm.h"
#include "tuner/algorithm/statictuner/statictuneralgorithm.h"
#include "tuner/algorithm/linearregression/linearregressiontuneralgorithm.h"

int main(int argc, char *argv[])
{
    // This is a simple improvement of the qDebug messages:
    #ifdef QT_DEBUG
    // In the debug mode verbose messages are shown including file and line number
    qSetMessagePattern("%{time h:mm:ss.zzz} \033[0m%{if-debug}Debug%{endif}%{if-info}Info%{endif}%{if-warning}WARNING%{endif}%{if-critical}CRITICAL%{endif}%{if-fatal}FATAL%{endif}: %{message}\n             \033[036m[%{function}] [%{file}:%{line}] [thread=%{threadid}]\033[0m\n");
    #else
    // In the release mode only short messages are shown in case of warnings, errors, and fatals
    qSetMessagePattern("\033[036m%{if-warning}WARNING: %{message}%{endif}%{if-critical}CRITICAL: %{message}%{endif}%{if-fatal}FATAL: %{message}%{endif}");
    #endif

    QCoreApplication::setOrganizationName(CFG_ORGANIZATIONNAME);
    QCoreApplication::setOrganizationDomain(CFG_ORGANIZATIONDOMAIN);
    QCoreApplication::setApplicationName(CFG_APPLICATIONNAME);
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QQuickStyle::setStyle("Material");

    QGuiApplication app(argc, argv);

    qmlRegisterSingletonType<QmlInterface>("MonteverdiApp", 1, 0, "MonteverdiApp", [](QQmlEngine *, QJSEngine *) -> QObject* {return new QmlInterface();});
    qmlRegisterSingletonType<TunerAlgorithmRunner>("MonteverdiApp", 1, 0, "TunerAlgorithmRunner", [](QQmlEngine *, QJSEngine *) -> QObject* {return TunerAlgorithmRunner::instance();});
    qmlRegisterType<MidiInputMidiOutputMode>("MonteverdiApp", 1, 0, "MidiInputMidiOutputMode");
    qmlRegisterType<MidiInputSynthesizerOutputMode>("MonteverdiApp", 1, 0, "MidiInputSynthesizerOutputMode");
    qmlRegisterType<Translator>("MonteverdiApp", 1, 0, "Translator");
    qmlRegisterType<FileReader>("MonteverdiApp", 1, 0, "FileReader");
    qmlRegisterUncreatableType<Tuner>("MonteverdiApp", 1, 0, "Tuner", "Tuner must be created by the algorithm mode");
    qmlRegisterUncreatableType<TunerAlgorithmWorker>("MonteverdiApp", 1, 0, "TunerAlgorithmWorker", "TunerAlgorithmWorker must be created by the algorithm mode");
    qmlRegisterUncreatableType<Synthesizer>("MonteverdiApp", 1, 0, "Synthesizer", "Synthesizer must be created by the algorithm mode");
    qRegisterMetaType<TunerState>("TunerState");
    qRegisterMetaType<NoteState>("NoteState");
    qRegisterMetaType<QVector<int16_t>>("QVector<int16_t>");

    // register algorithms
    qmlRegisterType<EqualTunerAlgorithm>("MonteverdiApp", 1, 0, "EqualTunerAlgorithm");
    qmlRegisterType<StaticTunerAlgorithm>("MonteverdiApp", 1, 0, "StaticTunerAlgorithm");
    qmlRegisterType<LinearRegressionTunerAlgorithm>("MonteverdiApp", 1, 0, "LinearRegressionTunerAlgorithm");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    // load translations
    Translator::init(app, engine, "monteverdi", ":/languages/translations");

    // execute the application
    return app.exec();
}
