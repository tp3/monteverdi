QT += quick midi multimedia quickcontrols2 svg
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    qmlinterface.cpp \
    tuner/tuner.cpp \
    tuner/tunerstate.cpp \
    tuner/notestate.cpp \
    tuner/midiinputmidioutputmode.cpp \
    tuner/applicationmode.cpp \
    synthesizer/synthesizer.cpp \
    synthesizer/waveformgenerator.cpp \
    tuner/midiinputsynthesizeroutputmode.cpp \
    tuner/algorithm/statictuner/statictuneralgorithm.cpp \
    units/cents.cpp \
    units/frequency.cpp \
    tuner/algorithm/tuneralgorithmrunner.cpp \
    midi/midioutputsweeper.cpp \
    tuner/algorithm/tuneralgorithmworker.cpp \
    tuner/algorithm/tuneralgorithm.cpp \
    tuner/algorithm/equaltuner/equaltuneralgorithm.cpp \
    tuner/algorithm/equaltuner/equaltuneralgorithmworker.cpp \
    tuner/algorithm/statictuner/statictuneralgorithmworker.cpp \
    tuner/algorithm/linearregression/linearregressiontuneralgorithm.cpp \
    tuner/algorithm/linearregression/linearregressiontuneralgorithmworker.cpp \
    system/translator.cpp \
    synthesizer/waveform.cpp \
    system/filereader.cpp

RESOURCES += qml/qml.qrc \
    resources/media/media.qrc

# Translations
RESOURCES += resources/translations/languages.qrc
TRANSLATIONS += \
resources/translations/monteverdi_en.ts \
resources/translations/monteverdi_de.ts
lupdate_only {
    SOURCES += $$files(qml/*.qml, true)
}

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    qmlinterface.h \
    tuner/tuner.h \
    tuner/tunerstate.h \
    tuner/notestate.h \
    tuner/midiinputmidioutputmode.h \
    tuner/applicationmode.h \
    synthesizer/synthesizer.h \
    synthesizer/waveformgenerator.h \
    tuner/midiinputsynthesizeroutputmode.h \
    tuner/algorithm/statictuner/statictuneralgorithm.h \
    units/cents.h \
    units/frequency.h \
    tuner/algorithm/tuneralgorithmrunner.h \
    midi/midioutputsweeper.h \
    tuner/algorithm/tuneralgorithmworker.h \
    tuner/algorithm/tuneralgorithm.h \
    tuner/algorithm/equaltuner/equaltuneralgorithm.h \
    tuner/algorithm/equaltuner/equaltuneralgorithmworker.h \
    tuner/algorithm/statictuner/statictuneralgorithmworker.h \
    tuner/algorithm/linearregression/linearregressiontuneralgorithm.h \
    tuner/algorithm/linearregression/linearregressiontuneralgorithmworker.h \
    system/translator.h \
    synthesizer/waveform.h \
    system/filereader.h \
    config.h

DISTFILES += \
    tuner/algorithm/statictuner/StaticTunerViewForm.ui.qml \
    tuner/algorithm/statictuner/StaticTunerView.qml \
    tuner/algorithm/equaltuner/EqualTunerViewForm.ui.qml \
    tuner/algorithm/equaltuner/EqualTunerView.qml \
    tuner/algorithm/linearregression/LinearRegressionTunerView.qml \
    uncrustify.cfg \
    tuner/algorithm/equaltuner/EqualTunerHelpForm.ui.qml \
    tuner/algorithm/equaltuner/EqualTunerHelp.qml

winrt {
    include(deployment/winrt.pri)
}

ios {
    include(deployment/ios.pri)
}
