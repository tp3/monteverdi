ICON=logo-1024.png

convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 20x20   20-1.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 40x40   20-2.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 60x60   20-3.png

convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 29x29   29-1.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 58x58   29-2.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 87x87   29-3.png

convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 40x40     40-1.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 80x80     40-2.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 120x120   40-3.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 160x160   40-4.png

convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 50x50     50-1.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 100x100   50-2.png

convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 57x57     57-1.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 114x114   57-2.png

convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 120x120   60-2.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 180x180   60-3.png

convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 72x72     72-1.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 144x144   72-2.png

convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 76x76     76-1.png
convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 152x152   76-2.png

convert $ICON -alpha Set -colorspace sRGB -define png:format=png32 -resize 167x167   83.5-2.png

