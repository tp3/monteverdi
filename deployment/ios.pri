ios {
    QMAKE_MAC_SDK = iphoneos

    QMAKE_INFO_PLIST = $$PWD/ios/Info.plist

    assets_catalogs.files = $$files($$PWD/ios/*.xcassets)
    QMAKE_ASSET_CATALOGS = $$files($$PWD/ios/*.xcassets)

    launch_xib.files = $$files($$PWD/ios/IntonationLaunchScreen.xib)
    QMAKE_BUNDLE_DATA += launch_xib

#    QMAKE_IOS_TARGETED_DEVICE_FAMILY = 2
} else {
    error(This file may only be included on iOS Builds!)
}
