winrt {
    MIDI_GM_LIBS = "Microsoft.Midi.GmDls\" MinVersion=\"1.0.0.0\" Publisher=\"CN=Microsoft Corporation, O=Microsoft Corporation, L=Redmond, S=Washington, C=US"
    WINRT_MANIFEST.dependencies += $$MIDI_GM_LIBS
    WINRT_MANIFEST.background = $${LITERAL_HASH}e5e5e5
    WINRT_MANIFEST.minVersion = "10.0.15063.0"
    WINRT_MANIFEST.maxVersionTested = "10.0.16299.0"

    CONFIG(debug, debug|release) {
        PLUGIN_DIR = $$OUT_PWD/debug
        PLUGIN_FILE=qtwinrt_midid.dll
    }
    else {
        PLUGIN_DIR = $$OUT_PWD/release
        PLUGIN_FILE=qtwinrt_midi.dll
    }

    PLUGIN_DIR ~= s,/,\\,g


    QMAKE_POST_LINK += \
        $$sprintf($$QMAKE_MKDIR_CMD, $$quote($$PLUGIN_DIR\\midi)) $$escape_expand(\\n\\t) \
        $$QMAKE_COPY $$quote($$(QTDIR)\\plugins\\midi\\$$PLUGIN_FILE) $$quote($$PLUGIN_DIR\\midi\\$$PLUGIN_FILE) $$escape_expand(\\n\\t)
} else {
    error(This file may only be included on WinRT Builds!)
}

