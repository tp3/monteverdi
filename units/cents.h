#ifndef CENTS_H
#define CENTS_H

#include <cmath>
#include <ostream>
#include <QDebug>

// 12 semitones * 100 cents / semitone = 1200 cents for an octave
struct Cents {
    explicit Cents(double cents = 0) : cents(cents) {}
    double cents;

    Cents abs() const {
        return Cents(std::abs(cents));
    }

    Cents operator-() const {
        return Cents(-cents);
    }

    Cents operator-(const Cents &c) const {
        return Cents(cents - c.cents);
    }

    Cents operator+(const Cents &c) const {
        return Cents(cents + c.cents);
    }

    Cents operator=(const Cents &c) {
        cents = c.cents;
        return *this;
    }

    bool operator==(const Cents &c) const {
        return cents == c.cents;
    }

    bool operator!=(const Cents &c) const {
        return cents != c.cents;
    }

    bool operator<(const Cents &c) const {
        return cents < c.cents;
    }

    bool operator<=(const Cents &c) const {
        return cents <= c.cents;
    }

    bool operator>(const Cents &c) const {
        return cents > c.cents;
    }

    bool operator>=(const Cents &c) const {
        return cents >= c.cents;
    }

    inline friend std::ostream& operator<< (std::ostream& stream, const Cents& c) {
        return stream << c.cents << "ct";
    }

    inline friend QDebug operator<<(QDebug dbg, const Cents &c) {
        dbg.nospace() << c.cents << "ct";
        return dbg.maybeSpace();
    }
};

inline Cents operator"" _ct (unsigned long long c) {
    return Cents(c);
}

inline Cents operator"" _ct (char32_t c) {
    return Cents(c);
}

inline Cents operator"" _ct (long double c) {
    return Cents(c);
}

#endif // CENTS_H
