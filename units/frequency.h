#ifndef FREQUENCY_H
#define FREQUENCY_H

#include "cents.h"

struct Frequency {
    explicit Frequency(double f = 0) : f(f) {}
    double f;  // frequency in Hz

    Frequency operator+(const Cents &c) const {
        // note this is approximative computation of 2 ^ (cent / 1200)
        return Frequency(f * (1. + 0.00057762265 * c.cents + 1.6682396E-7 * c.cents * c.cents));
    }

    friend Frequency operator *(double s, Frequency f) {
        return Frequency(s * f.f);
    }

    friend Frequency operator *(Frequency f, double s) {
        return Frequency(s * f.f);
    }
};

inline Frequency operator"" _Hz(long double f) {
    return Frequency(f);
}

#endif // FREQUENCY_H
