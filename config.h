#ifndef CONFIG_H
#define CONFIG_H

#include <QtGlobal>

//=============================================================================
//              Compile-time configuration of the application
//=============================================================================

//------------------------- Application name etc ------------------------------

#define CFG_APPLICATIONNAME "Monteverdi"
#define CFG_ORGANIZATIONNAME "University of Wuerzburg"
#define CFG_ORGANIZATIONDOMAIN "just-intonation.org"
#define CFG_APPLICATIONNAME_LOWERCASE "monteverdi"

//----------------------------- Version numbers -------------------------------

// To modify the version number update here and then call script upateVersion.h
#define CFG_APPLICATION_VERSION "2018.09.01"
#define CFG_ROLLING_APPLICATION_VERSION 1

#endif // CONFIG_H
