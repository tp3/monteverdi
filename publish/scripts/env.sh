set -e

cd $(dirname $0)
cd ../..

export BASE_DIR=$PWD
export SCRIPTS_DIR=$BASE_DIR/publish/scripts
export PATH=$PATH:$SCRIPTS_DIR
export DEPLOY_IOS=$BASE_DIR/deployment/ios

echo "Loaded env with BASE_DIR=$BASE_DIR"