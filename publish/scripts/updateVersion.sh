#   Script for updating the version number
#

set -e
: ${BASE_DIR?"env not loaded"}

. $SCRIPTS_DIR/loadVersion.sh

: <<'END'
################################ Doxygen ##################################

echo "Updating version number in the doxyfile"
sed -i.bak "s/= \"Version [[:digit:]\.]* ([0-9]*)\"/= \"Version $versionString ($versionRolling)\"/" $BASE_DIR/publish/doxygen/doxyfile 

############################# Installer ##################################
echo Update version and date in the Qt installer
sed -i.bak "s/<Version>[[:digit:]\.]*<\/Version>/<Version>$versionString<\/Version>/" $BASE_DIR/publish/installer/config/config.xml
sed -i.bak "s/<Version>[[:digit:]\.]*<\/Version>/<Version>$versionString<\/Version>/" $BASE_DIR/publish/installer/packages/org.justintonation.app/meta/package.xml
sed -i.bak "s/<ReleaseDate>[[:digit:]\-]*<\/ReleaseDate>/<ReleaseDate>$releaseDate<\/ReleaseDate>/" $BASE_DIR/publish/installer/packages/org.justintonation.app/meta/package.xml

echo Create the version.xml file in the installer directory 
echo "# Created by updateVersion.sh
#
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<version>
	<app rolling=\"$versionRolling\" string=\"$versionString\" />
</version>" > $BASE_DIR/publish/installer/version.xml

############################### ANDROID ###################################

echo "Updating AndroidManifest.xml"
sed -i.bak "s/android:versionName=\"[[:digit:]\.]*\"/android:versionName=\"$versionString\"/" $BASE_DIR/publish/platforms/android/AndroidManifest.xml
sed -i.bak "s/android:versionCode=\"[[:digit:]]*\"/android:versionCode=\"$versionRolling\"/" $BASE_DIR/publish/platforms/android/AndroidManifest.xml

END

################################## IOS ####################################

echo "Updating version number in $BASE_DIR/publish/platforms/ios/Info.plist"
perl -i.bak -p -000 -e 's/(<key>CFBundleShortVersionString<\/key>\s*<string>)[0-9\.]+(<\/string>)/$1$ENV{versionString}$2/' $DEPLOY_IOS/Info.plist
perl -i.bak -p -000 -e 's/(<key>CFBundleVersion<\/key>\s*<string>)[0-9\.]+(<\/string>)/$1$ENV{versionRolling}$2/' $DEPLOY_IOS/Info.plist

: <<'END'
################################## OSX ####################################

perl -i.bak -p -000 -e 's/(<key>CFBundleShortVersionString<\/key>\s*<string>)[0-9\.]+(<\/string>)/$1$ENV{versionString}$2/' $BASE_DIR/publish/platforms/macosx/Info.plist
perl -i.bak -p -000 -e 's/(<key>CFBundleVersion<\/key>\s*<string>)[0-9\.]+(<\/string>)/$1$ENV{versionRolling}$2/' $BASE_DIR/publish/platforms/macosx/Info.plist

################################## WINRT ####################################

echo "Updating version number in WINRT appxmanifest.xml"
sed -i.bak "s/ Version=\"[[:digit:]\.]*.0\"/ Version=\"$versionString.0\"/" $BASE_DIR/publish/platforms/windows/appxmanifest.xml
# There is no rolling version in the appx manifest

END

################################## QML ####################################

echo "Updating version number in About.qml"
sed -i.bak "s/versionDate: \"[[:digit:]\.]*\"/versionDate: \"$versionString\"/" $BASE_DIR/qml/About.qml

################################## SUSE ####################################

echo
echo "Missing: The opensuse repository"
echo


# # write it to the openSUSE build service files
# echo "Updating openSUSE build service files"
# sed -i.bak "s/^pkgver=[[:digit:]\.]*/pkgver=$versionString/" $BASE_DIR/publish/openSUSE_Build_Service/PKGBUILD
# sed -i.bak "s/^Version:[[:space:][:digit:]\.]*/Version: $versionString/" $BASE_DIR/publish/openSUSE_Build_Service/entropypianotuner.dsc
# sed -i.bak "s/^Version:[[:space:][:digit:]\.]*/Version: $versionString/" $BASE_DIR/publish/openSUSE_Build_Service/entropypianotuner.spec

echo
echo "Renaming of version numbers and rolling version finished"
echo



