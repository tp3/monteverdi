# Read the version from config.h

echo "Reading the actual version from config.h"

set -e
: ${BASE_DIR?"env not loaded"}


# read version from version.h
versionfile="$BASE_DIR/config.h"
if [ -f "$versionfile" ]
then
	echo "$versionfile found at $versionfile."
else
	echo "$versionfile not found at $versionfile"
	exit 1
fi

# Isolate version content and date

versioncontents=`cat $versionfile`
reVersionString="CFG_APPLICATION_VERSION[[:space:]]+\"([0-9\.]+)\""
reVersionRolling="CFG_ROLLING_APPLICATION_VERSION[[:space:]]+([0-9]+)"

# echo $versioncontents
# echo $reVersionString
# echo $reVersionRolling


releaseDate=$(date +"%Y-%m-%d")


if [[ $versioncontents =~ $reVersionString ]]; then
    export versionString=${BASH_REMATCH[1]}
    echo "Detected version: $versionString"
fi

if [[ $versioncontents =~ $reVersionRolling ]]; then
    export versionRolling=${BASH_REMATCH[1]}
    echo "Detected rolling version: $versionRolling"
fi

