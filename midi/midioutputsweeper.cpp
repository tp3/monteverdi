#include "midioutputsweeper.h"

#include <QDebug>
#include <QMidiMessageCreator>

MidiOutputSweeper::MidiOutputSweeper(QObject *parent)
    : QObject(parent)
    , mKeyPressed(128, false)
    , mMappedChannelIsUsed(16,false)
    , mKeyOfMappedChannel(16,-1)
    , mSequentialNumber(0)
    , mSequentialNumberOfMappedChannel(16,0)
    , mCurrentPitch(128,0_ct)
    , mInstrument(0)
{
    // initialize channels
    for (int i = 0; i < 16; ++i) if (!cDefaultDrumChannels.contains(i)) {
        mChannels << i;
    }
}

void MidiOutputSweeper::handleMidiMessage(const QMidiMessage &m) {
    if (m.isControlChange()) {
        // Ignore two subsequent messages of the same type (and values)
        if (mLastChannelModeMessage.equalBytes(m)) return;
        mLastChannelModeMessage = m;
        switch (m.byte1()) {
            case 121: // Reset all controllers
            case 123: // All notes off including release
                allNotesOff();
                break;
            case 0:
            case 0x40:
            case 0x41:
            case 0x42:
            case 0x43:
            case 0x44:
                controlChange(m.channel(), m.byte1(), m.byte2());
                break;
            default:
                break;
        }
    } else {
        mLastChannelModeMessage = QMidiMessage();
        // ignore all other events.
    }
}

void MidiOutputSweeper::handleNoteState(const NoteState &state) {
    if (state.pressed) {
        turnNoteOn(state.channel, state.key, state.intensity);
        tune(state.key, state.pitch);
    } else {
        turnNoteOff(state.channel, state.key);
    }
}

void MidiOutputSweeper::setInstrument (int instrument) {
    mInstrument = instrument & 0x7f;
}

void MidiOutputSweeper::turnNoteOn (int channel, int key, int volume) {
    if (volume==0)
    {
        turnNoteOff(channel,key);
        return;
    }

    int mappedChannel = -1;
    int lowestSequentialNumber = 0x7fffffff;

    // Look if the key is already in a channel
    for (int i = 0; i < mKeyOfMappedChannel.size(); ++i) {
        if (mKeyOfMappedChannel[i] == key) {
            mappedChannel = i;
            break;
        }
    }

    if (mappedChannel < 0) {
        // Look for oldest channel not in use, called noteChannel
        for (int c : mChannels) if (!mMappedChannelIsUsed[c])
        {
            if (mSequentialNumberOfMappedChannel[c] < lowestSequentialNumber)
            {
                lowestSequentialNumber = mSequentialNumberOfMappedChannel[c];
                mappedChannel = c;
            }
        }
    }

    // If all channels are used search the one with the lowest sequential number
    if (mappedChannel < 0)
    {
        lowestSequentialNumber = 0x7fffffff;
        for (int c : mChannels)
        {
            if (mSequentialNumberOfMappedChannel[c] < lowestSequentialNumber)
            {
                lowestSequentialNumber = mSequentialNumberOfMappedChannel[c];
                mappedChannel = c;
            }
        }
        if (mappedChannel < 0) qWarning() << "Search inconsistent";

        qDebug() << "Release key on mapped channel" << mappedChannel;
        playNote(false,mappedChannel,mKeyOfMappedChannel[mappedChannel],0);
    }

    // Now we have a free channel, the noteChannel
    mMappedChannelIsUsed[mappedChannel] = true;
    mKeyOfMappedChannel[mappedChannel] = key;
    mSequentialNumberOfMappedChannel[mappedChannel] = ++mSequentialNumber;
    playNote (true,mappedChannel,key,volume);
}

void MidiOutputSweeper::afterTouch (int channel, int key, int volume) {
    for (int c : mChannels) if ( mMappedChannelIsUsed[c] &&
                                 mKeyOfMappedChannel[c] == key)
    {
        emit sendMidiMessage(QMidiMessageCreator::aftertouch(c, key, volume));
    }
}

void MidiOutputSweeper::controlChange (int channel, int control, int value) {
    for (int c : mChannels) {
        emit sendMidiMessage(QMidiMessageCreator::controlChange(c, control, value));
    }
}

void MidiOutputSweeper::bankSelect(int bank) {
    for (quint8 c : mChannels) {
        emit sendMidiMessage(QMidiMessageCreator::controlChange(c, 0, 0));
        emit sendMidiMessage(QMidiMessageCreator::controlChange(c, 0x20, bank));
        emit sendMidiMessage(QMidiMessageCreator::programChange(c, mInstrument));
    }
}

void MidiOutputSweeper::turnNoteOff (int channel, int key) {
    for (int c : mChannels) if ( mMappedChannelIsUsed[c] &&
                                 mKeyOfMappedChannel[c] == key)
    {
        playNote(false,c,key,0);
        mMappedChannelIsUsed[c] = false;
        mKeyOfMappedChannel[c] = -1;
    }
}

void MidiOutputSweeper::allNotesOff() {
    // Send 'all notes off' on all Midi channels
    for (quint8 channel = 0; channel < 16; ++channel)
        emit sendMidiMessage(QMidiMessageCreator::allNotesOff(channel));

    resetAllControllers();
}

void MidiOutputSweeper::resetAllControllers() {
    // Send 'reset all controllers' on all Midi channels
    for (quint8 channel = 0; channel < 16; channel++)
        emit sendMidiMessage(QMidiMessageCreator::resetAllControllers(channel));
}

void MidiOutputSweeper::tune (int key, Cents pitch) {
    mCurrentPitch[key]=pitch;
    for (int c=0; c<16; c++)
        if (mMappedChannelIsUsed[c] && mKeyOfMappedChannel[c] == key)
        {
            setPitchBend(c,pitch);
        }
}

void MidiOutputSweeper::playNote (bool on, int mappedChannel, int key, int vol) {
    if (on) {
        if (!mKeyPressed[key]) {
            emit sendMidiMessage(QMidiMessageCreator::noteOn(mappedChannel, key, vol));
        }
        setPitchBend(mappedChannel, mCurrentPitch[key]);
    } else {
        if (mKeyPressed[key]) {
            emit sendMidiMessage(QMidiMessageCreator::noteOff(mappedChannel, key, vol));
        }
    }
    mKeyPressed[key] = on;
}

void MidiOutputSweeper::setPitchBend (int channel, Cents cents) {
    if (cents.abs() >= 200_ct) {
        qDebug() << "Cannot tune" << cents << "cents";
        return;
    }
    int pitchbend = qRound(8192 * (1+cents.cents/200));
    emit sendMidiMessage(QMidiMessageCreator::pitchBend(channel, pitchbend));
}

void MidiOutputSweeper::sendInitialMidiCommands() {
    // if (not pMidiMicrotonal->isWorking()) return;
    // emit pMidiMicrotonal->sendMidiEvent(pMidiMicrotonal->cLoopMarker);

    bankSelect(2);
    for (quint8 c : mChannels)
        emit sendMidiMessage(QMidiMessageCreator::programChange(c, mInstrument));
}
