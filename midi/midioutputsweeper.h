#ifndef MIDIOUTPUTSWEEPER_H
#define MIDIOUTPUTSWEEPER_H

#include <QObject>
#include <QMidiMessage>

#include "units/cents.h"
#include "tuner/notestate.h"

class MidiOutputSweeper : public QObject
{
    Q_OBJECT
public:
    MidiOutputSweeper(QObject *parent = 0);

    void setInstrument (int instrument);
    void turnNoteOn (int channel, int key, int volume);
    void turnNoteOff (int channel, int key);
    void afterTouch (int channel, int key, int volume);
    void controlChange (int channel, int control, int value);
    void bankSelect (int bank);
    void allNotesOff();
    void resetAllControllers();
    void localControl (bool on);
    void tune (int key, Cents pitch);

signals:
    void sendMidiMessage(const QMidiMessage &m);

public slots:
    void handleMidiMessage(const QMidiMessage &m);
    void handleNoteState(const NoteState &state);


    void sendInitialMidiCommands (void);
    void updateChannels(QVector<int> allowedChannels) {mChannels = allowedChannels;}

private:
    void playNote (bool on, int mappedChannel, int key, int vol);
    void setPitchBend (int channel, Cents cents);

private:
    QMidiMessage mLastChannelModeMessage;
    QVector<bool> mKeyPressed;                      ///< Is the key currently pressed
    QVector<bool> mMappedChannelIsUsed;             ///< Indicates usage of channel
    QVector<int> mKeyOfMappedChannel;               ///< Associated key
    int mSequentialNumber;                          ///< Increasing tag
    QVector<int> mSequentialNumberOfMappedChannel;  ///< Tag storage
    QVector<Cents> mCurrentPitch;                  ///< Pitch
    int mInstrument;                                ///< The selected instrument
    QVector<int> mChannels;                         ///< Non drums channels that may be used
    const QVector<int> cDefaultDrumChannels = {9, 10};  ///< Take out drum channel
};

#endif // MIDIOUTPUTSWEEPER_H
