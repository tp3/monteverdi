#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <QMutex>
#include <QDebug>
#include <QTranslator>
#include <QVector>
#include <QSettings>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

//=============================================================================
//                             Class Translator
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \class Translator
/// \brief Convenience class for translations
/// \details This class simplifies the handling of translations. It
/// automatically scans the available languages and allows the user to
/// select one of them out of a list. Languages can be changed without
/// restarting the application. If the user selects a particular language, it will
/// be remembered via QSettings on restart. The class can be instantiated
/// from Qml and connected to a Combo box.
///
/// - C++:<br>
///   - First initialize the translator by calling
///   `Translator::init(...)`, for example:<br>
///   `Translator::init(app,engine,"myappname",":/languages/translations");`
///   - Get the available languages by calling `QStringList languages()`
///   - Change the language by getting the index of the desired language in the
///    list and by calling `setIndex(int)`
/// - Qml:<br>
///   - Register the Translator class in C++ by calling:<br>
///    `qmlRegisterType<Translator> ("<QmlClassName>",1,0,"Translator");`
///   - Import the class in Qml by typing:<br> `import <QmlClassName> 1.0`
///   - Create an instance of the translator in Qml by typing <br>
///    `Translator { id: translator }`
///   - Attach e.g. a combo box as follows:<br>
///    `ComboBox {`<br>
///    `   model: translator.languages`<br>
///    `   currentIndex: translator.index`<br>
///    `   onCurrentIndexChanged: if (count>0) translator.index=currentIndex`<br>
///    `}`<br>
///    Note that the if (count>0) avoids binding loops during retranslation.
///
///////////////////////////////////////////////////////////////////////////////

class Translator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList languages READ languages NOTIFY languagesChanged)
    Q_PROPERTY(int index READ index WRITE setIndex NOTIFY indexChanged)
public:
    Translator();
    static void init (QGuiApplication &app, QQmlApplicationEngine &engine,
                      const QString &appname, const QString &translationpath);
    QStringList languages() { return mLanguages; }
    int index()             { return mIndex; }

signals:
    void languagesChanged();
    void indexChanged();

public slots:
    void setIndex (int index);

private slots:
    void setLanguages (QStringList languages);
private:
    QStringList mLanguages;
    int mIndex;
};

//=============================================================================
//                         Class TranslatorSingleton
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \brief Singleton class for managing translations
/// \details This is a singleton class which has to be initialized by calling
/// the static function TranslatorSingleton::init or, equivalently,
/// Translator::init(...)
///////////////////////////////////////////////////////////////////////////////

class TranslatorSingleton : public QObject
{
    Q_OBJECT
public:
    static TranslatorSingleton& getSingleton();
    void init (QGuiApplication &app, QQmlApplicationEngine &engine,
               const QString &appname, const QString &translationpath);
    QStringList getLanguages() { return mAvailableLanguages; }
    void selectLanguage (const int index);

signals:
    void listOfLanguagesChanged(QStringList);
    void selectedLanguageChanged(int index);

private:
    static TranslatorSingleton *singletonInstance;
    TranslatorSingleton(){}
    static QMutex singletonMutex;

    QGuiApplication *pApp;
    QQmlApplicationEngine *pEngine;
    QString mAppName;
    QString mTranslationsPath;
    QLocale mCurrentLocale;
    QTranslator mTranslator;
    QVector<QLocale> mAvailableLocales;
    QStringList mAvailableLanguages;
};


#endif // TRANSLATOR_H
