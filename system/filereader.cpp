#include "filereader.h"
#include <QFile>
#include <QDebug>

FileReader::FileReader(QObject *parent) : QObject(parent) {

}

QUrl FileReader::filePath() const {
    return m_filePath;
}

const QString &FileReader::fileContent() const {
    return m_fileContent;
}

void FileReader::setFilePath(QUrl filePath) {
    if (m_filePath == filePath) {
        return;
    }
    m_filePath = filePath;

    emit filePathChanged();

    setFileContent(loadContent(filePath));
}

void FileReader::setFileContent(const QString &fileContent) {
    if (m_fileContent == fileContent) {
        return;
    }
    m_fileContent = fileContent;

    emit fileContentChanged();
}

QString FileReader::loadContent(QUrl filePath) const {
    QString path = filePath.toString();
    if (path.startsWith("qrc")) {
        path = path.right(path.size() - 3);
    }
    QFile file(path);
    if (!file.exists()) {
        qWarning() << "File " << filePath << " does not exist";
        return "";
    }

    if (!file.open(QFile::ReadOnly)) {
        qWarning() << "File " << filePath << " could not be opened";
        return "";
    }

    return QString(file.readAll());
}
