#include <QDebug>
#include <QDir>

#include "translator.h"

//=============================================================================
//                             Class Translator
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of a translator
/// \details Creates an instance of a Translator and connects it
/// with the singleton class.
///////////////////////////////////////////////////////////////////////////////

Translator::Translator() : mLanguages(), mIndex(0)
{
    connect(&TranslatorSingleton::getSingleton(),&TranslatorSingleton::listOfLanguagesChanged,
            this,&Translator::setLanguages);
    connect(&TranslatorSingleton::getSingleton(),&TranslatorSingleton::selectedLanguageChanged,
            this,&Translator::setIndex);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Static function for the initialization of the translator
/// \param app : Reference to the current QGuiApplication
/// \param engine : Reference to the current Qml engine
/// \param appname : Lowercase name of the application, used for translation
/// file name, e.g. for "myapp" the translation files are "myapp_de.ts"
/// \param translationpath : Path or resource path to the translation files
/// \details This function initializes the translation singleton class.
///////////////////////////////////////////////////////////////////////////////

void Translator::init(QGuiApplication &app, QQmlApplicationEngine &engine, const QString &appname, const QString &translationpath)
{
    TranslatorSingleton::getSingleton().init(app,engine,appname,translationpath);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Set the index property
/// \param index : Index of the language in the list of available languages.
/// \details Use this slot to change a language.
///////////////////////////////////////////////////////////////////////////////

void Translator::setIndex (int index)
{
    if (mIndex==index) return;
    mIndex = index;
    TranslatorSingleton::getSingleton().selectLanguage(index);
    emit indexChanged();
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Update the list of available languages
/// \param languages : QStringList of available languages
/// \details This slot is called internally from the TranlatorSingleton class.
///////////////////////////////////////////////////////////////////////////////

void Translator::setLanguages(QStringList languages)
{
    if (mLanguages==languages) return;
    mLanguages=languages;
    emit languagesChanged();
}


//=============================================================================
//                         Class TranslatorSingleton
//=============================================================================

TranslatorSingleton* TranslatorSingleton::singletonInstance = nullptr;
QMutex TranslatorSingleton::singletonMutex;


///////////////////////////////////////////////////////////////////////////////
/// \brief Return a pointer to the TranslatorSingleton class
/// \return Pointer to the instance of the singleton class
///////////////////////////////////////////////////////////////////////////////

TranslatorSingleton &TranslatorSingleton::getSingleton()
{
    QMutexLocker lock(&singletonMutex);
    if (!singletonInstance) singletonInstance=new TranslatorSingleton();
    return *singletonInstance;
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Initialization of the translator.
/// \details This function initializes the TranslatorSingleton instance.
/// It searches the translationpath for installed translation files and
/// loads the most recently used language from QSettings (empty for automatic).
/// \param app : Reference to the current QGuiApplication
/// \param engine : Reference to the current Qml engine
/// \param appname : Lowercase name of the application, used for translation
/// file name, e.g. for "myapp" the translation files are "myapp_de.ts"
/// \param translationpath : Path or resource path to the translation files
///////////////////////////////////////////////////////////////////////////////

void TranslatorSingleton::init(QGuiApplication &app, QQmlApplicationEngine &engine,
                       const QString &appname, const QString &translationpath)
{
    pApp = &app;
    pEngine = &engine;
    mAppName = appname;
    mTranslationsPath = translationpath;

    // Create a vector of all installed locales
    mAvailableLocales.clear();
    mAvailableLanguages.clear();
    mAvailableLocales += QLocale::system();
    mAvailableLanguages += tr("Automatic");
    QStringList filenames = QDir(mTranslationsPath).entryList(QStringList("*.qm"));
    for (QString &filename : filenames)
    {
        filename.truncate(filename.lastIndexOf('.'));  // Strip off .qm
        filename.remove(0, filename.indexOf('_') + 1); // Strip off *_
        QLocale locale(filename);
        mAvailableLocales.push_back(locale);
        QString languageName = QLocale::system().languageToString(locale.language());
        mAvailableLanguages.push_back(languageName +  " - " + locale.nativeLanguageName());
    }
    emit listOfLanguagesChanged(mAvailableLanguages);
    qInfo() << "Installed translations:" << mAvailableLanguages;

    // Get the most recently used language abbreviation from settings (""=automatic)
    QSettings settings;
    QString lastLanguage = settings.value("app/locale").toString();
    if (lastLanguage.isEmpty()) selectLanguage(0);
    else selectLanguage(mAvailableLocales.indexOf(QLocale(lastLanguage)));
}


///////////////////////////////////////////////////////////////////////////////
/// \brief Select a different language
/// \param index : Index of the language in the list of languages()
///////////////////////////////////////////////////////////////////////////////

void TranslatorSingleton::selectLanguage(const int index)
{
    if (index < 0) selectLanguage(0);
    QLocale locale;
    if (index > 0) locale = mAvailableLocales[index];
    if (locale == mCurrentLocale) return;
    qInfo() << "Select language" << locale;

    // Construct translation filename
    QString fileName = locale.name();
    if (fileName.contains("_")) fileName.truncate(fileName.lastIndexOf('_'));
    fileName = mAppName + "_" + fileName + ".qm";

    // Replace QTranslator
    pApp->removeTranslator(&mTranslator);
    if (!mTranslator.load(fileName,mTranslationsPath))
    {
        qWarning() << "Could not load translations for" << fileName;
        return;
    }
    if (!pApp->installTranslator(&mTranslator))
    {
        qWarning() << "Could not install translations for" << fileName;
        return;
    }
    mCurrentLocale = locale;
    pEngine->retranslate();
    emit selectedLanguageChanged(index);

    // Update settings:
    QSettings settings;
    if (index==0) settings.setValue("app/locale","");
    else settings.setValue("app/locale",mCurrentLocale.name());
}


