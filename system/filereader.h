#ifndef FILEREADER_H
#define FILEREADER_H

#include <QObject>
#include <QUrl>

class FileReader : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QUrl filePath READ filePath WRITE setFilePath NOTIFY filePathChanged)
    Q_PROPERTY(QString fileContent READ fileContent WRITE setFileContent NOTIFY fileContentChanged)
public:
    explicit FileReader(QObject *parent = nullptr);

    QUrl filePath() const;
    const QString &fileContent() const;
signals:
    void filePathChanged();
    void fileContentChanged();

public slots:
    void setFilePath(QUrl filePath);
    void setFileContent(const QString &fileContent);

private:
    QString loadContent(QUrl filePath) const;

    QUrl m_filePath;
    QString m_fileContent;
};

#endif // FILEREADER_H
