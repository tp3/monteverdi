#ifndef APPLICATIONMODE_H
#define APPLICATIONMODE_H

#include <QObject>

#include "tuner.h"

class ApplicationMode : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Tuner* tuner READ tuner)

public:
    ApplicationMode(QObject *parent = 0);

    Tuner *tuner() {return m_tuner;}
    const Tuner *tuner() const {return m_tuner;}

protected:
    Tuner *m_tuner;
};

#endif // APPLICATIONMODE_H
