#include "tunerstate.h"
#include <QDebug>


TunerState::TunerState()
    : m_tunerState(MAX_NUM_KEYS)
{
    for (int i = 0; i < m_tunerState.size(); ++i) {
        m_tunerState[i].key = static_cast<quint8>(i);
    }
}

void TunerState::setNoteState(const NoteState &state) {
    if (m_tunerState[state.key] != state) {
        m_tunerState[state.key] = state;
        emit tunerStateChanged(state);
    }
}

const NoteState &TunerState::noteState(int key) const {
    if (key >= 0 && key < m_tunerState.size()) {
        return m_tunerState[key];
    } else {
        qWarning() << "Invalid range for key: " << key;
    }
    return m_tunerState[0];
}
