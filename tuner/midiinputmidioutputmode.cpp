#include "midiinputmidioutputmode.h"
#include "QMidiMessageCreator"
#include "midi/midioutputsweeper.h"

MidiInputMidiOutputMode::MidiInputMidiOutputMode(QObject *parent)
    : ApplicationMode(parent)
{
    connect(this, &MidiInputMidiOutputMode::handleMidiMessage, m_tuner, &Tuner::recevieMidiMessage);

    MidiOutputSweeper *sweeper = new MidiOutputSweeper(this);
    connect(m_tuner, &Tuner::outputNoteChanged, sweeper, &MidiOutputSweeper::handleNoteState);
    connect(m_tuner, &Tuner::unprocessedMidiMessage, sweeper, &MidiOutputSweeper::handleMidiMessage);
    connect(sweeper, &MidiOutputSweeper::sendMidiMessage, this, &MidiInputMidiOutputMode::outputMidiMessage);
}

void MidiInputMidiOutputMode::outputNoteChanged(const NoteState &note) {
    if (note.pressed) {
        emit outputMidiMessage(QMidiMessageCreator::noteOn(note.channel, note.key, note.intensity));
    } else {
        emit outputMidiMessage(QMidiMessageCreator::noteOff(note.channel, note.key, 0));
    }
}
