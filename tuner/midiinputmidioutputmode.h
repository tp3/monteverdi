#ifndef MIDIINPUTMIDIOUTPUTMODE_H
#define MIDIINPUTMIDIOUTPUTMODE_H

#include <QMidiMessage>

#include "tunerstate.h"

#include "applicationmode.h"

class MidiInputMidiOutputMode : public ApplicationMode
{
    Q_OBJECT

public:
    MidiInputMidiOutputMode(QObject *parent = 0);

signals:
    void handleMidiMessage(const QMidiMessage &m);
    void outputMidiMessage(const QMidiMessage &message);

private slots:
    void outputNoteChanged(const NoteState &note);
};

#endif // MIDIINPUTMIDIOUTPUTMODE_H
