#include "midiinputsynthesizeroutputmode.h"
#include "synthesizer/synthesizer.h"

MidiInputSynthesizerOutputMode::MidiInputSynthesizerOutputMode(QObject *parent)
    : ApplicationMode(parent)
    , m_synthesizer(new Synthesizer(this))
{
    connect(this, &MidiInputSynthesizerOutputMode::handleMidiMessage, m_tuner, &Tuner::recevieMidiMessage);
    connect(m_tuner, &Tuner::outputNoteChanged, this, &MidiInputSynthesizerOutputMode::outputNoteChanged);
    // unprocessed midi messages are ignored
    // connect(m_tuner, &Tuner::unprocessedMidiMessage, [this](QMidiMessage message) {emit outputMidiMessage(message);});
}

void MidiInputSynthesizerOutputMode::outputNoteChanged(const NoteState &note) {
    QMidiMessage m;
    if (note.pressed) {
        m_synthesizer->playNote(note.key, note.pitch, note.intensity / 128);
    } else {
        m_synthesizer->playNote(note.key, note.pitch, 0);
    }

    // launch the synthesizer
    emit outputMidiMessage(m);
}
