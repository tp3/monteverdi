#ifndef LINEARREGRESSIONTUNERALGORITHMWORKER_H
#define LINEARREGRESSIONTUNERALGORITHMWORKER_H

#include "../tuneralgorithmworker.h"
#include <QVector>

#include "3rdparty/eigen/Eigen/Eigen"

class LinearRegressionTunerAlgorithmWorker : public TunerAlgorithmWorker
{
    Q_OBJECT
public:
    LinearRegressionTunerAlgorithmWorker();

signals:
    void deviationChanged(double deviation);

public slots:
    void setInterval(int index, double value) {m_intervals[index] = value;}
    void setWeight(int index, double value) {m_weights[index] = value;}
    void setOptimized(bool optimized) {m_optimizedJI = optimized;}

private:
    virtual void run(TunerState &tunerState) override;

private:
    QVector<double> m_intervals;
    QVector<double> m_weights;
    bool m_optimizedJI;

    Eigen::MatrixXi mSelectedVariant;           ///< Previously used pitch variant
    double mProgression;                        ///< Actual pitch progression

    const double octaveWeightFactor = 0.5;
    const double memoryWeight = 0.00005;
    const double epsilon = 1E-10;


    // The following array holds the list of pitch alternatives for
    // the twelve intervals in Just Intonation:
    const QVector<QVector<double>> cJustIntonation =
        {{0},                           ///< unison and octaves
         {11.7313,-7.821},              ///< semitone 16/15 and 135/128
         {3.9100, -17.5963},            ///< second 9/8 and 10/9
         {15.6413},                     ///< minor third 6/5
         {-13.6863},                    ///< major third 5/4
         {-1.9550},                     ///< perfect fourth 4/3
         {-9.77628},                    ///< tritone 45/32
         {1.9559},                      ///< perfect fifth 3/2
         {13.6863},                     ///< minor sixth 8/5
         {-15.6413},                    ///< major sixth 5/3
         {-3.9100, 17.5963, -31.1741},  ///< minor seventh 16/9, 9/5, and natural seventh 7/4
         {-11.7313}};                   ///< major seventh 15/8
};

#endif // LINEARREGRESSIONTUNERALGORITHMWORKER_H
