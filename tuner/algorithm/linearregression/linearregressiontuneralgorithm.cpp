#include "linearregressiontuneralgorithm.h"
#include "linearregressiontuneralgorithmworker.h"

LinearRegressionTunerAlgorithm::LinearRegressionTunerAlgorithm(QObject *parent)
    : TunerAlgorithm(TunerAlgorithmRunner::LINEAR_REGRESSION_TUNER_ALGORITHM,
                     m_worker = new LinearRegressionTunerAlgorithmWorker(),
                     parent)
    , m_intervals(12, 0)
    , m_weights(12, 1)
    , m_optimized(true)
{
    QObject::connect(this, &LinearRegressionTunerAlgorithm::intervalChanged, m_worker, &LinearRegressionTunerAlgorithmWorker::setInterval, Qt::QueuedConnection);
    QObject::connect(this, &LinearRegressionTunerAlgorithm::weightChanged, m_worker, &LinearRegressionTunerAlgorithmWorker::setWeight, Qt::QueuedConnection);
}

void LinearRegressionTunerAlgorithm::setInterval(int key, double value) {
    if (key < 0 || key >= m_intervals.size()) {return;}
    if (m_intervals[key] != value) {
        m_intervals[key] = value;
        emit intervalChanged(key, value);
    }
}

void LinearRegressionTunerAlgorithm::setWeight(int key, double value) {
    if (key < 0 || key >= m_weights.size()) {return;}
    if (m_weights[key] != value) {
        m_weights[key] = value;
        emit weightChanged(key, value);
    }
}

void LinearRegressionTunerAlgorithm::setOptimized(bool optimized) {
    if (m_optimized != optimized) {
        m_optimized = optimized;
        emit optimizedChanged(m_optimized);
    }
}
