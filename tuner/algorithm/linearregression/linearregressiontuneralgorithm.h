#ifndef LINEARREGRESSIONTUNERALGORITHM_H
#define LINEARREGRESSIONTUNERALGORITHM_H

#include "../tuneralgorithm.h"

class LinearRegressionTunerAlgorithmWorker;

class LinearRegressionTunerAlgorithm : public TunerAlgorithm
{
    Q_OBJECT

    Q_PROPERTY(bool optimized READ optimized WRITE setOptimized NOTIFY optimizedChanged)
public:
    LinearRegressionTunerAlgorithm(QObject *parent = nullptr);


    const QVector<double> &intervalse() const {return m_intervals;}
    const QVector<double> &weights() const {return m_weights;}
    bool optimized() const {return m_optimized;}

signals:
    void intervalChanged(int key, double value);
    void weightChanged(int key, double value);
    void optimizedChanged(bool optimized);

public slots:
    void setInterval(int key, double value);
    void setWeight(int key, double value);
    void setOptimized(bool optimized);

private:
    QVector<double> m_intervals;
    QVector<double> m_weights;
    bool m_optimized;

    LinearRegressionTunerAlgorithmWorker *m_worker;
};

#endif // LINEARREGRESSIONTUNERALGORITHM_H
