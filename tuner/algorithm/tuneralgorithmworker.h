#ifndef TUNERALGORITHMWORKER_H
#define TUNERALGORITHMWORKER_H

#include <QObject>
#include <QElapsedTimer>

#include "../tunerstate.h"
#include "tuneralgorithmrunner.h"

class TunerAlgorithmWorker : public QObject
{
    Q_OBJECT
public:
    TunerAlgorithmWorker();

    bool isRunning() const {return m_running;}


public slots:
    void tune(TunerState tunerState);

signals:
    void tuningFinished(TunerState tunerState);

protected:
    virtual void run(TunerState& TunerState) = 0;

private:
    std::atomic<bool> m_running;
    QElapsedTimer m_timer;

};

#endif // TUNERALGORITHMWORKER_H
