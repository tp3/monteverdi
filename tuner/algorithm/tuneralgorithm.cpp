#include "tuneralgorithm.h"
#include "tuneralgorithmworker.h"
#include "tuneralgorithmrunner.h"

TunerAlgorithm::TunerAlgorithm(TunerAlgorithmRunner::AlgorithmTypes type,
                               TunerAlgorithmWorker *worker,
                               QObject *parent)
    : QObject(parent)
    , m_type(type)
    , m_algorithm(worker)
{
    m_thread = new QThread;
    m_algorithm->moveToThread(m_thread);
    QObject::connect(m_algorithm, &TunerAlgorithmWorker::tuningFinished, this, &TunerAlgorithm::tuningFinished);

    m_thread->start();

    // register ourself as the new algorithm
    TunerAlgorithmRunner::instance()->setTunerAlgorithm(this);
}

TunerAlgorithm::~TunerAlgorithm() {
    m_thread->quit();
    m_thread->wait();
    delete m_thread;
    delete m_algorithm;
}

bool TunerAlgorithm::isRunning() const {
    return m_algorithm->isRunning();
}

void TunerAlgorithm::tune(TunerState tunerState) {
    if (!m_algorithm->isRunning()) {
        QMetaObject::invokeMethod(m_algorithm, "tune", Qt::QueuedConnection, Q_ARG(TunerState,  tunerState));
    }
}
