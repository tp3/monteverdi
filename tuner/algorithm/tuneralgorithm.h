#ifndef TUNERALGORITHM_H
#define TUNERALGORITHM_H

#include <QObject>
#include <QThread>

#include "tuneralgorithmrunner.h"
#include "../tunerstate.h"

class TunerAlgorithmWorker;

class TunerAlgorithm : public QObject
{
    Q_OBJECT

    Q_PROPERTY(TunerAlgorithmWorker *algorithm READ algorithm)
public:
    TunerAlgorithm(TunerAlgorithmRunner::AlgorithmTypes type, TunerAlgorithmWorker *worker, QObject *parent = 0);

    virtual ~TunerAlgorithm();

    TunerAlgorithmRunner::AlgorithmTypes type() const {return m_type;}
    bool isRunning() const;

    TunerAlgorithmWorker* algorithm() {return m_algorithm;}

signals:
    void tuningFinished(TunerState tunerState);

public slots:
    void tune(TunerState tunerState);

private:
    const TunerAlgorithmRunner::AlgorithmTypes m_type;
    TunerAlgorithmWorker *m_algorithm;
    QThread *m_thread;
};

#endif // TUNERALGORITHM_H
