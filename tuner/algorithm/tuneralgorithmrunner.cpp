#include "tuneralgorithmrunner.h"
#include "tuneralgorithmworker.h"
#include "tuneralgorithm.h"

TunerAlgorithmRunner *TunerAlgorithmRunner::THE_ONE_AND_ONLY;

TunerAlgorithmRunner::TunerAlgorithmRunner()
    : m_type(UNINITIALIZED_TUNER_ALGORITHM)
    , m_algorithm(nullptr)
{
}

TunerAlgorithmRunner::~TunerAlgorithmRunner() {
}

bool TunerAlgorithmRunner::isRunning() const {
    if (m_algorithm) {
        return m_algorithm->isRunning();
    }
    return false;
}

void TunerAlgorithmRunner::setTunerAlgorithm(TunerAlgorithm *algorithm) {
    m_algorithm = algorithm;
    if (algorithm) {
        m_type = m_algorithm->type();
        QObject::connect(algorithm, SIGNAL(destroyed(QObject*)), this, SLOT(tunerAlgorithmDeleted(QObject*)));
        QObject::connect(m_algorithm, &TunerAlgorithm::tuningFinished, this, &TunerAlgorithmRunner::tuningFinished, Qt::QueuedConnection);
    } else {
        m_type = UNINITIALIZED_TUNER_ALGORITHM;
    }
    emit algorithmChanged();
}

void TunerAlgorithmRunner::tune(TunerState tunerState) {
    if (m_algorithm) {
        m_algorithm->tune(tunerState);
    }
}

void TunerAlgorithmRunner::tunerAlgorithmDeleted(QObject *object) {
    if (m_algorithm == object) {
        setTunerAlgorithm(nullptr);
    }
}
