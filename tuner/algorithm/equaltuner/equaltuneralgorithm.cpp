#include "equaltuneralgorithm.h"
#include "equaltuneralgorithmworker.h"

EqualTunerAlgorithm::EqualTunerAlgorithm(QObject *parent)
    : TunerAlgorithm(TunerAlgorithmRunner::EQUAL_TEMPERAMENT_TUNER_ALGORITHM,
                     new EqualTunerAlgorithmWorker,
                     parent)
{
}
