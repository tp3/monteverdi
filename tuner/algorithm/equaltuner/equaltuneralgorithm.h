#ifndef EQUALTUNERALGORITHM_H
#define EQUALTUNERALGORITHM_H

#include "../tuneralgorithm.h"

class EqualTunerAlgorithm : public TunerAlgorithm
{
public:
    EqualTunerAlgorithm(QObject *parent = 0);
};

#endif // EQUALTUNERALGORITHM_H
