#ifndef EQUALTUNERALGORITHMWORKER_H
#define EQUALTUNERALGORITHMWORKER_H

#include "../tuneralgorithmworker.h"

class EqualTunerAlgorithmWorker : public TunerAlgorithmWorker
{
public:
    EqualTunerAlgorithmWorker();

private:
    virtual void run(TunerState &tunerState) override;
};

#endif // EQUALTUNERALGORITHMWORKER_H
