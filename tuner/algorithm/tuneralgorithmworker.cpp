#include "tuneralgorithmworker.h"
#include "tuneralgorithmrunner.h"
#include <QThread>

TunerAlgorithmWorker::TunerAlgorithmWorker()
    : QObject(nullptr)  // no parent cause this object will be anyways in a separate thread
    , m_running(false)
{
    m_timer.start();
}

void TunerAlgorithmWorker::tune(TunerState tunerState) {
    if (m_running) {return;}
    m_running = true;

    // sleep for at least 10 ms, or update every 50 ms
    int ms = m_timer.elapsed();
    QThread::msleep(std::max(20, 50 - ms));
    m_timer.start();

    run(tunerState);
    m_running = false;
    emit tuningFinished(tunerState);
}
