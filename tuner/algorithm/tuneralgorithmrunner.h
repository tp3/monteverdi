#ifndef TUNERALGORITHMRUNNER_H
#define TUNERALGORITHMRUNNER_H

#include <memory>
#include <QObject>
#include <QThread>

#include "tuner/tunerstate.h"

class TunerAlgorithm;

class TunerAlgorithmRunner : public QObject
{
    Q_OBJECT

    Q_PROPERTY(AlgorithmTypes algorithmType READ type NOTIFY algorithmChanged)

private:
    static TunerAlgorithmRunner* THE_ONE_AND_ONLY;
    TunerAlgorithmRunner();

public:
    static TunerAlgorithmRunner *instance() {
        if (!THE_ONE_AND_ONLY) {
            THE_ONE_AND_ONLY = new TunerAlgorithmRunner;
        }
        return THE_ONE_AND_ONLY;
    }

public:
    enum AlgorithmTypes {
        UNINITIALIZED_TUNER_ALGORITHM          = 0,
        EQUAL_TEMPERAMENT_TUNER_ALGORITHM      = 1,
        STATIC_TEMPERAMENT_TUNER_ALGORITHM     = 2,
        LINEAR_REGRESSION_TUNER_ALGORITHM      = 3,
    };

    Q_ENUMS(AlgorithmTypes)

public:
    virtual ~TunerAlgorithmRunner();

    AlgorithmTypes type() const { return m_type; }
    bool isRunning() const;
    void setTunerAlgorithm(TunerAlgorithm* algorithm);

signals:
    void tuningFinished(TunerState tunerState);
    void algorithmChanged();

public slots:
    void tune(TunerState tunerState);
    void tunerAlgorithmDeleted(QObject *object);

private:
    AlgorithmTypes m_type;
    TunerAlgorithm* m_algorithm;
};

#endif // TUNERALGORITHMRUNNER_H
