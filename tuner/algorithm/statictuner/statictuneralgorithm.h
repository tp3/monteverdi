#ifndef STATICTUNERALGORITHM_H
#define STATICTUNERALGORITHM_H

#include "../tuneralgorithm.h"

class StaticTunerAlgorithmWorker;


class StaticTunerAlgorithm : public TunerAlgorithm {
    Q_OBJECT

    Q_PROPERTY(QVector<Cents> pitches READ pitches WRITE setPitches NOTIFY pitchesChanged)
    Q_PROPERTY(int referenceKey READ referenceKey WRITE setReferenceKey NOTIFY referenceKeyChanged)
    Q_PROPERTY(int wolfsIntervalShift READ wolfsIntervalShift WRITE setWolfsIntervalShift NOTIFY wolfsIntervalShiftChanged)

public:
    StaticTunerAlgorithm(QObject *parent = 0);
    virtual ~StaticTunerAlgorithm();

    const QVector<Cents> &pitches() const {return m_pitches;}
    int referenceKey() const {return m_referenceKey;}
    int wolfsIntervalShift() const {return m_wolfsIntervalShift;}

    Q_INVOKABLE void setPitch(int index, double cent);
    Q_INVOKABLE double getPitch(int index);

signals:
    void pitchChanged(int index, double cent);
    void pitchesChanged();
    void referenceKeyChanged();
    void wolfsIntervalShiftChanged();

public slots:
    void setPitches(const QVector<Cents> &pitches);
    void setReferenceKey(int key);
    void setWolfsIntervalShift(int shift);

private:
    StaticTunerAlgorithmWorker *m_worker;
    QVector<Cents> m_pitches;
    int m_referenceKey = 0;
    int m_wolfsIntervalShift = 0;
};

#endif // STATICTUNERALGORITHM_H
