#include "statictuneralgorithm.h"

#include <QDebug>

#include "statictuneralgorithmworker.h"

StaticTunerAlgorithm::StaticTunerAlgorithm(QObject *parent)
    : TunerAlgorithm(TunerAlgorithmRunner::STATIC_TEMPERAMENT_TUNER_ALGORITHM,
                     m_worker = new StaticTunerAlgorithmWorker(),
                     parent)
    , m_pitches(12, Cents(0))
{

    m_worker->setReferenceKey(m_referenceKey);
    m_worker->setWolfsIntervalShift(m_wolfsIntervalShift);
}

StaticTunerAlgorithm::~StaticTunerAlgorithm() {
}

void StaticTunerAlgorithm::setPitch(int index, double cent) {
    if (index < 0 || index >= m_pitches.size()) { return; }
    if (m_pitches[index] != Cents(cent)) {
        m_pitches[index] = Cents(cent);
        QMetaObject::invokeMethod(m_worker, "setPitch", Qt::QueuedConnection, Q_ARG(int, index), Q_ARG(double, cent));
        emit pitchChanged(index, cent);
        emit pitchesChanged();
    }
}

double StaticTunerAlgorithm::getPitch(int index) {
    if (index < 0 || index >= m_pitches.size()) {
        qWarning() << "Invalid index " << index;
        return 0;
    } else {
        return m_pitches[index].cents;
    }
}

void StaticTunerAlgorithm::setPitches(const QVector<Cents> &pitches) {
    if (pitches.size() != m_pitches.size()) {
        qWarning() << "Inconsistent pitches size!";
    }
    for (int i = 0; i < m_pitches.size(); ++i) {
        setPitch(i, pitches[i].cents);
    }
}

void StaticTunerAlgorithm::setReferenceKey(int key) {
    if (m_referenceKey != key) {
        m_referenceKey = key;
        QMetaObject::invokeMethod(m_worker, "setReferenceKey", Qt::QueuedConnection, Q_ARG(int,  key));
        emit referenceKeyChanged();
    }
}

void StaticTunerAlgorithm::setWolfsIntervalShift(int shift) {
    if (m_wolfsIntervalShift != shift) {
        m_wolfsIntervalShift = shift;
        QMetaObject::invokeMethod(m_worker, "setWolfsIntervalShift", Qt::QueuedConnection, Q_ARG(int, shift));
        emit wolfsIntervalShiftChanged();
    }
}
