#ifndef STATICTUNERALGORITHMWORKER_H
#define STATICTUNERALGORITHMWORKER_H

#include "../tuneralgorithmworker.h"

class StaticTunerAlgorithmWorker : public TunerAlgorithmWorker
{
    Q_OBJECT

public:
    StaticTunerAlgorithmWorker();

public slots:
    void setPitch(int index, double cents) {m_pitches[index] = Cents(cents);}
    void setReferenceKey(int key) {m_referenceKey = key;}
    void setWolfsIntervalShift(int shift) {m_wolfsIntervalShift = shift;}

private:
    virtual void run(TunerState &tunerState) override;

    QVector<Cents> m_pitches;
    int m_referenceKey = 0;
    int m_wolfsIntervalShift = 0;

    void tuneStatically(TunerState &tunerState,
                        QVector<Cents> pitches,
                        const int referenceKey,
                        const int wolfsIntervalShift);
};

#endif // STATICTUNERALGORITHMWORKER_H
