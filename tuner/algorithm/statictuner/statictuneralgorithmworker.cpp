#include "statictuneralgorithmworker.h"

StaticTunerAlgorithmWorker::StaticTunerAlgorithmWorker()
    : m_pitches(12, Cents(0))
{
    // create dummy fixed tuning on C major.
    m_referenceKey = 0;
}

void StaticTunerAlgorithmWorker::run(TunerState &tunerState) {
    tuneStatically(tunerState, m_pitches, m_referenceKey, m_wolfsIntervalShift);
}

void StaticTunerAlgorithmWorker::tuneStatically(TunerState &tunerState, QVector<Cents> pitches, const int referenceKey, const int wolfsIntervalShift) {
    for (int i = 0; i < tunerState.size(); ++i) {
        auto note = tunerState.noteState(i);
        if (note.intensity > 0) {
            int distance = (10 * 12 + note.key - referenceKey - 1 + wolfsIntervalShift) % 12;
            note.pitch = pitches[distance];
            tunerState.setNoteState(note);
        }
    }
}
