#ifndef TUNERSTATE_H
#define TUNERSTATE_H

#include <QVector>

#include "notestate.h"

class TunerState : public QObject
{
    Q_OBJECT

public:
    static constexpr int MAX_NUM_KEYS = 128;

public:
    TunerState();
    TunerState(const TunerState &state)
        : QObject(0) {
        m_tunerState = state.m_tunerState;
    }

    Q_INVOKABLE const NoteState &noteState(int key) const;
    int size() {return m_tunerState.size();}

    const QVector<NoteState> &states() const { return m_tunerState; }

signals:
    void tunerStateChanged(const NoteState &state);

public slots:
    void setNoteState(const NoteState &state);


private:
    QVector<NoteState> m_tunerState;
};

Q_DECLARE_METATYPE(TunerState)

#endif // TUNERSTATE_H
