#include "tuner.h"

#include <QDateTime>
#include <QVariant>
#include <QDebug>

#include "algorithm/tuneralgorithmrunner.h"


Tuner::Tuner(QObject *parent)
    : QObject(parent)
    , m_tunerAlgorithm(TunerAlgorithmRunner::instance())  // no this as parent, different threads!
{

    connect(&m_outputState, &TunerState::tunerStateChanged, this, &Tuner::outputNoteChanged);

    connect(m_tunerAlgorithm, &TunerAlgorithmRunner::tuningFinished, this, &Tuner::tunerFinished);
    connect(m_tunerAlgorithm, &TunerAlgorithmRunner::algorithmChanged, this, &Tuner::tunerChanged);
}

Tuner::~Tuner() {
}

void Tuner::tunerFinished(TunerState state) {
    for (const NoteState &note : state.states()) {
        m_outputState.setNoteState(note);
    }

    while (m_unprocessedMidiMessageDuringTuning.size() > 0) {
        emit unprocessedMidiMessage(m_unprocessedMidiMessageDuringTuning.dequeue());
    }

    startTuner(m_inputState);
}

void Tuner::tunerChanged() {
    // restart the tuning process
    startTuner(m_inputState);
}

void Tuner::recevieMidiMessage(const QMidiMessage &message) {
    m_inputMessages.enqueue(message);

    const quint64 current = QDateTime::currentMSecsSinceEpoch();
    while (m_inputMessages.size() > 0 && m_inputMessages.head().timestamp() <= current) {
        processMidiMessage(m_inputMessages.dequeue());
    }

    startTuner(m_inputState);
}


void Tuner::processMidiMessage(const QMidiMessage &message) {
    if (message.isNoteOn() || message.isNoteOff()) {
        NoteState state = m_inputState.noteState(message.key());
        state.channel = message.channel();
        state.newlyPressed = true;
        state.pressed = message.isNoteOn();
        state.timeStamp = message.timestamp();
        state.intensity = message.velocity() * message.isNoteOn();
        m_inputState.setNoteState(state);
    } else {
        m_unprocessedMidiMessageDuringTuning.enqueue(message);
    }
}

void Tuner::startTuner(TunerState state) {
    if (!m_tunerAlgorithm->isRunning()) {
        m_tunerAlgorithm->tune(state);
    }
}
