#ifndef TUNER_H
#define TUNER_H

#include <QQueue>
#include <QtMidi/QMidiMessage>
#include <QThread>
#include <memory>

#include "tunerstate.h"

class TunerAlgorithmRunner;

/**
 * @brief The Tuner class
 *
 * Management of the whole tuning process.
 *
 * It expect midi messages as input (connect to receiveMidiMessage) and outputs either:
 *   - Unprocessed midi messages (unprocessedMidiMessage). Currently only note on/off are processed.
 *   - Note State changes of a note (e. g. pitch change, but also on/off, velocity, ...)
 *
 * A Mode should handle and further process note changes (e. g. send as midi message) and all
 * unprocessed midi messages. E. g. (todo) send it to the 'midi channel sweeper'.
 *
 * Internally it calls the tuning process continously.
 *
 * The tuner keeps the current input state (up to date with 'now') and the output state, i.e. changes made by the tuning algorithm.
 *
 */
class Tuner : public QObject
{
    Q_OBJECT

public:
    Tuner(QObject *parent = 0);
    virtual ~Tuner();

signals:
    void unprocessedMidiMessage(const QMidiMessage &message);
    void outputNoteChanged(const NoteState &state);


public slots:
    void recevieMidiMessage(const QMidiMessage &message);

private slots:
    void tunerFinished(TunerState state);
    void tunerChanged();

private:
    void processMidiMessage(const QMidiMessage &message);

    void startTuner(TunerState state);

private:
    TunerState m_inputState;                                        ///< Input state (wanted state of the device)
    TunerState m_outputState;                                       ///< Output state (target state of the device)

    QQueue<QMidiMessage> m_inputMessages;                           ///< Input messages to process
    QQueue<QMidiMessage> m_unprocessedMidiMessageDuringTuning;      ///< Unprocessed midi message that will be sent on finish of tuning algorithm
    TunerAlgorithmRunner *m_tunerAlgorithm;                         ///< The actual tuning algorithm
};

#endif // TUNER_H
