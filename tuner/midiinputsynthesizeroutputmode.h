#ifndef MIDIINPUTSYNTHESIZEROUTPUTMODE_H
#define MIDIINPUTSYNTHESIZEROUTPUTMODE_H

#include "applicationmode.h"

class Synthesizer;

class MidiInputSynthesizerOutputMode : public ApplicationMode
{
    Q_OBJECT

    Q_PROPERTY(Synthesizer* synthesizer READ synthesizer NOTIFY synthesizerChanged)

public:
    MidiInputSynthesizerOutputMode(QObject * parent = nullptr);

    Synthesizer *synthesizer() {return m_synthesizer;}

signals:
    void handleMidiMessage(const QMidiMessage &m);
    void outputMidiMessage(const QMidiMessage &message);

    void synthesizerChanged();  // this is a dummy signal required for qml

private slots:
    void outputNoteChanged(const NoteState &note);

private:
    Synthesizer *m_synthesizer;
};

#endif // MIDIINPUTSYNTHESIZEROUTPUTMODE_H
