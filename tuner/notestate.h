#ifndef NOTESTATE_H
#define NOTESTATE_H

#include <QObject>

#include "units/cents.h"

class NoteState
{
    Q_GADGET
public:
    quint8 key = 0;                     ///< Number of the key
    quint8 channel = 0;                 ///< Channel
    bool pressed = false;               ///< Flag indicating a pressed key
    bool newlyPressed = false;          ///< Flag indicating a newly pressed key
    bool emitted = false;               ///< Flag indicating first emitted tuning
    double intensity = 0;               ///< Intensity (volume) I(t)
    double memory = 0;                  ///< Psychoacoustic memory M(t)
    double sustainDampingFactor = 1;    ///< Sustain damping of the key
    double releaseDampingFactor = 0;    ///< Release damping of the key
    qint64 timeStamp = 0;               ///< Time when the key was pressed
    Cents pitch = 0.0_ct;               ///< Actual pitch
    Cents previousPitch = 0.0_ct;       ///< Previously emitted pitch

    Q_PROPERTY(quint8 key MEMBER key)
    Q_PROPERTY(quint8 channel MEMBER channel)
    Q_PROPERTY(bool pressed MEMBER pressed)
    Q_PROPERTY(bool newlyPressed MEMBER newlyPressed)
    Q_PROPERTY(bool emitted MEMBER emitted)
    Q_PROPERTY(double intensity MEMBER intensity)
    Q_PROPERTY(double memory MEMBER memory)
    Q_PROPERTY(double sustainDampingFactor MEMBER sustainDampingFactor)
    Q_PROPERTY(double releaseDampingFactor MEMBER releaseDampingFactor)
    Q_PROPERTY(qint64 timeStamp MEMBER timeStamp)
    Q_PROPERTY(Cents pitch MEMBER pitch)
    Q_PROPERTY(Cents previousPitch MEMBER previousPitch)

    bool operator==(const NoteState &state) const {
        return key == state.key
                && channel == state.channel
                && pressed == state.pressed
                && newlyPressed == state.newlyPressed
                && emitted == state.emitted
                && intensity == state.intensity
                && memory == state.memory
                && sustainDampingFactor == state.sustainDampingFactor
                && releaseDampingFactor == state.releaseDampingFactor
                && timeStamp == state.timeStamp
                && pitch == state.pitch
                && previousPitch == state.previousPitch;
    }

    bool operator !=(const NoteState &state) const {
        return key != state.key
                || channel != state.channel
                || pressed != state.pressed
                || newlyPressed != state.newlyPressed
                || emitted != state.emitted
                || intensity != state.intensity
                || memory != state.memory
                || sustainDampingFactor != state.sustainDampingFactor
                || releaseDampingFactor != state.releaseDampingFactor
                || timeStamp != state.timeStamp
                || pitch != state.pitch
                || previousPitch != state.previousPitch;
    }
};

#endif // NOTESTATE_H
