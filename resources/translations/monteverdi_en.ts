<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../../qml/About.qml" line="7"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/About.qml" line="10"/>
        <source>Monteverdi The App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/About.qml" line="11"/>
        <source>Build %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/About.qml" line="13"/>
        <source>based on %1, and %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutForm.ui</name>
    <message>
        <location filename="../../qml/AboutForm.ui.qml" line="66"/>
        <source>The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EqualTunerHelpForm.ui</name>
    <message>
        <location filename="../../qml/algorithms/EqualTunerHelpForm.ui.qml" line="15"/>
        <source>Equal Tuner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/EqualTunerHelpForm.ui.qml" line="21"/>
        <source>This tuning is the common tuning in todays western music: Any semitone is equidistant. In this tuning scheme you can play any key alike on the drawback that there are no pure intervalls.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EqualTunerViewForm.ui</name>
    <message>
        <location filename="../../qml/algorithms/EqualTunerViewForm.ui.qml" line="12"/>
        <source>The differences in frequency between any semitone is fixed. This is the usual tuning of modern western music.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../../qml/help/Help.qml" line="6"/>
        <source>Help - Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/Help.qml" line="47"/>
        <source>Introduction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/Help.qml" line="51"/>
        <source>Midi In Midi Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/Help.qml" line="55"/>
        <source>MIDI Input Synthesizer Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/Help.qml" line="59"/>
        <source>Virtual Keyboard Input Synthesizer Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/Help.qml" line="63"/>
        <source>Equal Tuning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/Help.qml" line="67"/>
        <source>Static Tuning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/Help.qml" line="71"/>
        <source>Dynamic Tuning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <location filename="../../qml/help/HelpPage.qml" line="7"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HomeForm.ui</name>
    <message>
        <location filename="../../qml/HomeForm.ui.qml" line="7"/>
        <source>Home</source>
        <translation type="unfinished">Home</translation>
    </message>
</context>
<context>
    <name>IntroductionHelpForm.ui</name>
    <message>
        <location filename="../../qml/help/IntroductionHelpForm.ui.qml" line="15"/>
        <source>Introduction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/IntroductionHelpForm.ui.qml" line="21"/>
        <source>This app offers a solution to the problem that on a piano it is impossible to play all keys in pure intonation. A dynamic tuning algorithm detects the played keys and slightly chanes its pitches to receive frequency ratios of the intervals that are pure if possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/IntroductionHelpForm.ui.qml" line="29"/>
        <source>The app is best used with a connected MIDI keyboard: The pressed keys are sent to the app and processed to determine the small pitch variations. This new tuning is sent back to the keyboard to generate the sound in pure intonation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinearRegressionTunerHelpForm.ui</name>
    <message>
        <location filename="../../qml/algorithms/LinearRegressionTunerHelpForm.ui.qml" line="21"/>
        <source>This tuner uses a sophisticated algorithm to automatically tune the played keys to pure intervals as close as possible. Thus it is possible to dynamically change the key and still play in Just Intonation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinearRegressionTunerView</name>
    <message>
        <location filename="../../qml/algorithms/LinearRegressionTunerView.qml" line="16"/>
        <source>An algorithm will automatically adjust the temperament so that it is as close to a pure intonation as possible.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../../qml/MainMenu.qml" line="9"/>
        <source>Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenu.qml" line="10"/>
        <source>Temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenu.qml" line="11"/>
        <source>Settings and help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainMenuListModel</name>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="20"/>
        <location filename="../../qml/MainMenuListModel.qml" line="33"/>
        <source>MIDI In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="21"/>
        <source>MIDI Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="23"/>
        <source>Connect the app with your MIDI keyboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="34"/>
        <location filename="../../qml/MainMenuListModel.qml" line="47"/>
        <source>Synth Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="36"/>
        <source>Play a synthesized sound controlled by a MIDI keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="46"/>
        <source>Virtual Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="49"/>
        <source>Play a synthesized sound controlled by a Virtual onscreen keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="59"/>
        <source>Equal temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="62"/>
        <source>Standard keyboard temperament. Possible to play in all keys but impure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="72"/>
        <source>Static temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="75"/>
        <source>Select a temperament relative to one fixed key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="85"/>
        <source>Dynamic temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="88"/>
        <source>Dynamically adjust the key depending on the currently played keys.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="98"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="101"/>
        <source>Change the default program settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="111"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="114"/>
        <source>Get help about the app and just intonation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="124"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/MainMenuListModel.qml" line="127"/>
        <source>Information about the application and disclaimer.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="../../qml/MessageBox.qml" line="21"/>
        <source>Information message.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MidiInputMidiOutputHelpForm.ui</name>
    <message>
        <location filename="../../qml/help/MidiInputMidiOutputHelpForm.ui.qml" line="7"/>
        <source>MIDI In MIDI Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/MidiInputMidiOutputHelpForm.ui.qml" line="20"/>
        <source>Select MIDI In MIDI Out if you intend to connect the app to your midi keyboard or to any other MIDI devices that shall be used for input or output. This is the typical use case of this app.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/MidiInputMidiOutputHelpForm.ui.qml" line="28"/>
        <source>&lt;ol&gt;&lt;li&gt;Connect your MIDI keyboard with a MIDI cable to your device.&lt;/li&gt;&lt;li&gt;Check if the app automatically selects your device as new input and output MIDI device.&lt;/li&gt;&lt;li&gt;Use your keyboard to make music&lt;/li&gt;&lt;/ol&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MidiInputMidiOutputView</name>
    <message>
        <location filename="../../qml/connection/MidiInputMidiOutputView.qml" line="62"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/connection/MidiInputMidiOutputView.qml" line="63"/>
        <source>MIDI device was disconnected before it was reset to the original state. As a result your device might be muted. You should reconnect the device and disconnect the switch first.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MidiInputMidiOutputViewForm.ui</name>
    <message>
        <location filename="../../qml/connection/MidiInputMidiOutputViewForm.ui.qml" line="56"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/connection/MidiInputMidiOutputViewForm.ui.qml" line="56"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MidiInputSynthesizerOutputHelpForm.ui</name>
    <message>
        <location filename="../../qml/help/MidiInputSynthesizerOutputHelpForm.ui.qml" line="7"/>
        <source>MIDI In Synthesizer Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/MidiInputSynthesizerOutputHelpForm.ui.qml" line="20"/>
        <source>This connection mode can be used to connect a MIDI keyboard but play a generated sound by the app.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../../qml/Settings.qml" line="5"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsForm.ui</name>
    <message>
        <location filename="../../qml/SettingsForm.ui.qml" line="20"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StaticTunerCentsView</name>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="8"/>
        <source>Interval Cents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="24"/>
        <source>Semitone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="24"/>
        <source>Major Second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="25"/>
        <source>Minor Third</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="25"/>
        <source>Major Third</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="25"/>
        <source>Perfect Forth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="26"/>
        <source>Tritone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="26"/>
        <source>Perfect Fifth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="26"/>
        <source>Minor Sixth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="27"/>
        <source>Major Sixth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="27"/>
        <source>Minor Seventh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="27"/>
        <source>Major Seventh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="28"/>
        <source>Octave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="49"/>
        <source>Temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="56"/>
        <source>Equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="56"/>
        <source>Just Intonation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="56"/>
        <source>Pythagorean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="56"/>
        <source>1/4 Meantone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="56"/>
        <source>Werckmeister III</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="56"/>
        <source>Silbermann 1/6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="56"/>
        <source>Zarline -2/7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerCentsView.qml" line="56"/>
        <source>User defined</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StaticTunerHelpForm.ui</name>
    <message>
        <location filename="../../qml/algorithms/StaticTunerHelpForm.ui.qml" line="21"/>
        <source>This tuing scheme will set fixed intervall ratios based on one key. Thus, you can play pure, but only in one key.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerHelpForm.ui.qml" line="29"/>
        <source>Besides Just Intonation many different temperaments are predefined.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StaticTunerView</name>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="63"/>
        <source>Specify a reference key for a static tuning. Several different tuning schemes (e.g. just intonation) are available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="77"/>
        <source>Reference Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="85"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="85"/>
        <source>C#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="85"/>
        <source>D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="85"/>
        <source>D#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="85"/>
        <source>E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="86"/>
        <source>F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="86"/>
        <source>F#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="86"/>
        <source>G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="86"/>
        <source>G#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="86"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="87"/>
        <source>A#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="87"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="104"/>
        <source>Temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="111"/>
        <source>Equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="111"/>
        <source>Just Intonation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="111"/>
        <source>Pythagorean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="111"/>
        <source>1/4 Meantone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="111"/>
        <source>Werckmeister III</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="111"/>
        <source>Silbermann 1/6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="111"/>
        <source>Zarline -2/7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="111"/>
        <source>User defined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/algorithms/StaticTunerView.qml" line="140"/>
        <source>Edit intervalls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../system/translator.cpp" line="113"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VirtualKeyboardInputSynthesizerOutputHelpForm.ui</name>
    <message>
        <location filename="../../qml/help/VirtualKeyboardInputSynthesizerOutputHelpForm.ui.qml" line="16"/>
        <source>Virtual Keyboard In Synthesizer Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/help/VirtualKeyboardInputSynthesizerOutputHelpForm.ui.qml" line="21"/>
        <source>If you have no MIDI keyboard available but still want to try the different  tunings of this application a virtual on-screen keyboard is offered. The sounds will be produced by an inbuild sound generator.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VirtualMidiKeyboardInputSynthesizerOutputViewForm.ui</name>
    <message>
        <location filename="../../qml/connection/VirtualMidiKeyboardInputSynthesizerOutputViewForm.ui.qml" line="17"/>
        <source>Open Virutal Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VirtualMidiKeyboardView</name>
    <message>
        <location filename="../../qml/connection/VirtualMidiKeyboardView.qml" line="6"/>
        <source>Virtual Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../qml/main.qml" line="12"/>
        <source>Monteverdi - The - App</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
