<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>EqualTunerViewForm.ui</name>
    <message>
        <location filename="../../tuner/algorithm/equaltuner/EqualTunerViewForm.ui.qml" line="9"/>
        <source>No parameters available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HomeForm.ui</name>
    <message>
        <location filename="../../HomeForm.ui.qml" line="5"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../HomeForm.ui.qml" line="8"/>
        <source>You are on the home page.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="../../MessageBox.qml" line="21"/>
        <source>Information message.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MidiInputMidiOutputView</name>
    <message>
        <location filename="../../MidiInputMidiOutputView.qml" line="60"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MidiInputMidiOutputView.qml" line="61"/>
        <source>MIDI device was disconnected before it was reset to the original state. As a result your device might be muted. You should reconnect the device and disconnect the switch first.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MidiInputMidiOutputViewForm.ui</name>
    <message>
        <location filename="../../MidiInputMidiOutputViewForm.ui.qml" line="8"/>
        <source>MIDI In → MIDI Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MidiInputMidiOutputViewForm.ui.qml" line="33"/>
        <source>↪</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MidiInputMidiOutputViewForm.ui.qml" line="55"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MidiInputMidiOutputViewForm.ui.qml" line="55"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MidiInputMidiOutputViewForm.ui.qml" line="71"/>
        <source>↩</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MidiInputSynthesizerOutputViewForm.ui</name>
    <message>
        <location filename="../../MidiInputSynthesizerOutputViewForm.ui.qml" line="8"/>
        <source>MIDI In → Synthesizer Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MidiInputSynthesizerOutputViewForm.ui.qml" line="32"/>
        <source>↪</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MidiInputSynthesizerOutputViewForm.ui.qml" line="62"/>
        <source>↩</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MidiInputSynthesizerOutputViewForm.ui.qml" line="69"/>
        <source>Output Device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Page1Form.ui</name>
    <message>
        <location filename="../../Page1Form.ui.qml" line="10"/>
        <source>Page 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Page1Form.ui.qml" line="13"/>
        <source>You are on Page 1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StaticTunerView</name>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="42"/>
        <source>Semitone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="42"/>
        <source>Major Second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="43"/>
        <source>Minor Third</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="43"/>
        <source>Major Third</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="43"/>
        <source>Perfect Forth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="44"/>
        <source>Tritone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="44"/>
        <source>Perfect Fifth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="44"/>
        <source>Minor Sixth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="45"/>
        <source>Major Sixth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="45"/>
        <source>Minor Seventh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="45"/>
        <source>Major Seventh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="46"/>
        <source>Octave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="80"/>
        <source>Reference Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="88"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="88"/>
        <source>C#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="88"/>
        <source>D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="88"/>
        <source>D#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="88"/>
        <source>E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="89"/>
        <source>F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="89"/>
        <source>F#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="89"/>
        <source>G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="89"/>
        <source>G#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="89"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="90"/>
        <source>A#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="90"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="107"/>
        <source>Temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="114"/>
        <source>Equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="114"/>
        <source>Just Intonation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="114"/>
        <source>Pythagorean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="114"/>
        <source>1/4 Meantone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="114"/>
        <source>Werckmeister III</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="114"/>
        <source>Silbermann 1/6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="114"/>
        <source>Zarline -2/7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../tuner/algorithm/statictuner/StaticTunerView.qml" line="114"/>
        <source>User defined</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../system/translator.cpp" line="113"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VirtualMidiKeyboardInputSynthesizerOutputViewForm.ui</name>
    <message>
        <location filename="../../VirtualMidiKeyboardInputSynthesizerOutputViewForm.ui.qml" line="6"/>
        <source>Virtual Keyboard -&gt; MIDI Out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../main.qml" line="12"/>
        <source>Monteverdi - The - App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.qml" line="72"/>
        <source>Algorithm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.qml" line="79"/>
        <source>Equal Temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.qml" line="79"/>
        <source>Static Temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.qml" line="79"/>
        <source>Dynamic Temperament</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.qml" line="136"/>
        <source>MIDI In → MIDI Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.qml" line="144"/>
        <source>MIDI In → Synthesizer Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.qml" line="152"/>
        <source>Virtual Keyboard In -&gt; Synthesizer Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.qml" line="160"/>
        <source>Page 2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
