To install an additional language proceed as follows:
-----------------------------------------------------

1. Get the ISO code (e.g. fr for French)

2. Open main project file and add 
   TRANSLATIONS += resources/translations/monteverdi_fr.ts 

3. Open languages.qrc and add the line 
   <file>monteverdi_fr.qm</file>

4. Run qmake

5. Run Extras->Extern->Linguist->lupdate
   This should create the new monteverdi_fr.ts file
   
6. Run Qt/<version>/gcc_64/bin/linguist monteverdi_fr.ts
   and insert the translations
   
7. Run Qt/<version>/gcc_64/bin/lrelease application.pro
